#! /usr/bin/env node

import { argv, exit } from 'process';
import { spawn } from 'child_process';
import { URL } from 'url';

const TIMEOUT = 20;
const dirname = new URL('.', import.meta.url).pathname;

/**
 * @param {string} ipaType
 * @param {number} timeout
 * @returns {Promise<string[]>}
 */
async function deviceScan(ipaType, timeout) {
  console.error(`Scanning for ${timeout} seconds...`);

  const result = await new Promise((resolve, reject) => {
    const iosdeployBinary = `${dirname}/../node_modules/.bin/ios-deploy`;
    const iosdeployParams = ['--detect', '--timeout', timeout];
    const child = spawn(iosdeployBinary, iosdeployParams);

    const resultItems = [];

    child.on('close', (code) => {
      if (code === 0) {
        resolve(resultItems.sort());
      } else {
        reject(new Error(code));
      }
    });

    child.on('error', (error) => {
      reject(error);
    });

    child.stdout.on('data', (buffer) => {
      buffer.toString().trim().split(/\n/).forEach((output) => {
        const parsedOutput = output.match(/^\[\.\.\.\.\] Found ([0-9a-fA-F-]+) .+?a\.k\.a\. '(.+)'/);
        if (parsedOutput) {
          // eslint-disable-next-line no-unused-vars
          const [_, uuid, name] = parsedOutput;
          resultItems.push(`${name}:${uuid}:${ipaType}`);
        }
      });
    });

    child.stderr.on('data', (buffer) => {
      const output = buffer.toString().trim();
      console.error(output);
    });
  });

  console.error(`Found ${result.length} devices`);
  console.error('Copy this to your device file:\n');
  console.log(result.join('\n'));

  return result;
}

if (!argv[2]) {
  console.error('\nUsage: ./scripts/get_devices.js ipaTypeId\nipaTypeId corresponds to the ipaTypes in your Redux config.\n');
  exit(1);
}

deviceScan(argv[2], TIMEOUT)
  .catch(console.error);
