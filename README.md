# Redux iOS Sign & Deploy Tool

A MacOS based tool to (re-) sign and mass-deploy IPAs to a list of iOS based devices via USB or Wi-Fi.

It can be used to manage mass deployments of iOS software for automated tests, distributed iPhone based computing or similar purposes.

## Prerequisites & Installation

* Install Homebrew, the MacOS package manager: See [install instructions](https://docs.brew.sh/Installation).
* Install NodeJS v16 or newer: `brew install node`.
    * Confirm NodeJS version: `node --version`.
    * In case Homebrew did not install NodeJS v16 or newer, install nvm (`brew install nvm`), follow the setup instructions, then use `nvm install 16`.x
* Install libimobiledevice: `brew install libimobiledevice ideviceinstaller`.
* Install Redux:
    * Clone this repository: `git clone https://gitlab.com/redux-tools/redux-deployer.git`.
    * Enter the created working copy directory: `cd redux-deployer`.
    * Install the Redux's dependencies: `npm install`.

## Configuration

All settings are stored in a YAML file at `config/settings.yaml`, the template `config/settings.template.yaml` contains detailed configuration instructions.

### Device List

Redux requires a device database in the `deployment:devices` section of the settings. This is usually a text file like this:

```
name:UUID:defaultIpaType
name:UUID:defaultIpaType
name:UUID:defaultIpaType
```

`UUID` is the iOS devices unique ID, `defaultIpaType` is the default IPA type to install on your device (referring to the IPA type ID in the Apps list).

To quickly get the list of devices connected to your Mac, you can run `scripts/get_devices.js defaultIpaType` which will output a list in the required format.

### Apps List

Redux requires a list of available deployable/removable apps to be specified in the `ipaTypes` section of the settings. Contact your Redux admin for more details.

### IPA specific configuration

Depending on your IPA type, a config file is expected in the respective `config/ipa/{ipatype}` folder.

## Usage

```
./redux.js [-r] [-c] [-f] [options]

Mode of operation:

-r       Restart all devices.
-c       Remove IPA from all devices.
-f       Full Auto mode: Grab the latest IPAs from the corresponding endpoints, sign them and deploy them to all devices.

The mode of operation flags can be combined. If no mode is specified, Redux starts in an interactive mode.

Options:

-d filename  Use this device list, defaults to config/devices.txt
-v version   Modify the version of the used IPAs.
-l           Don't download latest IPA, but use locally available.
-i filename  Use this local IPA file.
-u url       Download IPA from this specific URL.
-n filename  Download IPA to this local file.
```
