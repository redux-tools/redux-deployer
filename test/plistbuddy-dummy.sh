#! /bin/sh --posix

if [ "x$1" = x-x -a "x$2" = x-c  ]; then
  case "$3" in
    print\ :exit1*)
      echo "Some error" 1>&2
      exit 1
    ;;
    print\ :exit2*)
      exit 2
    ;;
    add\ :alreadyexisting*)
      echo "Add: :alreadyexisting Entry Already Exists" 1>&2
      exit 1
    ;;
    delete\ :nonexisting*)
      echo "Delete: Entry, :nonexisting Does Not Exist" 1>&2
      exit 1
    ;;
  esac
  echo "did something"
  exit 0
fi

cat <<EOF
Usage: PlistBuddy [-cxh] <file.plist>
    -c "<command>" execute command, otherwise run in interactive mode
    -x output will be in the form of an xml plist where appropriate
    -h print the complete help info, with command guide

EOF
