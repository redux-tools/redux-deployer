import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import IosDeploy from './IosDeploy.js';
import TimeoutError from './TimeoutError.js';

use(chaiAsPromised);
use(sinonChai);

describe('IosDeploy', () => {
  describe('#install()', () => {
    it('should eventually succeed if valid binary is invoked', () => {
      const deployer = new IosDeploy({ iosdeployBinary: './test/ios-deploy-dummy.sh' });

      const params = { uuid: 'abc', ipa: 'normal.ipa', timeout: 1 };
      const promise = deployer.install(params);

      return expect(promise).to.be.fulfilled;
    });

    it('should emit a 0% and 100% progress event', async () => {
      const deployer = new IosDeploy({ iosdeployBinary: './test/ios-deploy-dummy.sh' });

      const progressSpy = sinon.spy();
      deployer.on('progress', progressSpy);

      const params = { uuid: 'abc', ipa: 'normal.ipa' };
      // @ts-ignore
      await deployer.install(params);

      expect(progressSpy).to.be.calledWithMatch({ percentage: 0.0 });
      expect(progressSpy).to.be.calledWithMatch({ percentage: 1.0 });
    });

    it('should fail with the exit code on ios-deploy error', () => {
      const deployer = new IosDeploy({ iosdeployBinary: './test/ios-deploy-dummy.sh' });

      const errorSpy = sinon.spy();
      deployer.on('error', errorSpy);

      const params = { uuid: 'abc', ipa: 'error.ipa' };
      const promise = deployer.install(params);

      return expect(promise).to.be.rejected.then(() => {
        expect(errorSpy).to.be.calledWithMatch({ message: 'Lost connection' });
      });
    });

    it('should reject with TimeoutError if times out', () => {
      const deployer = new IosDeploy({ iosdeployBinary: './test/ios-deploy-dummy.sh' });

      const params = { uuid: 'abc', ipa: 'timeout.ipa' };
      const promise = deployer.install(params);

      return expect(promise).to.be.rejectedWith(TimeoutError);
    });

    it('should fail if invalid binary is invoked', () => {
      const deployer = new IosDeploy({ iosdeployBinary: 'non-existing-binary' });

      const params = { uuid: 'abc', ipa: 'normal.ipa' };
      const promise = deployer.install(params);

      return expect(promise).to.eventually.be.rejected;
    });
  });

  describe('#uninstall()', () => {
    it('should eventually succeed if valid binary is invoked', () => {
      const iosdeploy = new IosDeploy({ iosdeployBinary: './test/ios-deploy-dummy.sh' });

      const params = { uuid: 'abc', bundle: 'def', timeout: 1 };
      const promise = iosdeploy.uninstall(params);

      return expect(promise).to.be.fulfilled;
    });

    it('should emit a 100% progress event at the end', async () => {
      const iosdeploy = new IosDeploy({ iosdeployBinary: './test/ios-deploy-dummy.sh' });

      const progressSpy = sinon.spy();
      iosdeploy.on('progress', progressSpy);

      const params = { uuid: 'abc', bundle: 'def' };
      await iosdeploy.uninstall(params);

      expect(progressSpy).to.be.calledWithMatch({ percentage: 0.0 });
      expect(progressSpy).to.be.calledWithMatch({ percentage: 1.0 });
    });

    it('should fail if invalid binary is invoked', () => {
      const iosdeploy = new IosDeploy({ iosdeployBinary: 'non-existing-binary' });

      const params = { uuid: 'abc', bundle: 'def' };
      const promise = iosdeploy.uninstall(params);

      return expect(promise).to.eventually.be.rejected;
    });
  });

  describe('#installProvisioningProfile()', () => {
    it('should eventually succeed if valid binary is invoked', () => {
      const deployer = new IosDeploy({ iosdeployBinary: './test/ios-deploy-dummy.sh' });

      const params = { uuid: 'abc', provisioningProfile: 'def.mobileprovision', timeout: 1 };
      const promise = deployer.installProvisioningProfile(params);

      return expect(promise).to.be.fulfilled;
    });

    it('should emit a 0% and 100% progress event', async () => {
      const deployer = new IosDeploy({ iosdeployBinary: './test/ios-deploy-dummy.sh' });

      const progressSpy = sinon.spy();
      deployer.on('progress', progressSpy);

      const params = { uuid: 'abc', provisioningProfile: 'def.mobileprovision' };
      // @ts-ignore
      await deployer.installProvisioningProfile(params);

      expect(progressSpy).to.be.calledWithMatch({ percentage: 0.0 });
      expect(progressSpy).to.be.calledWithMatch({ percentage: 1.0 });
    });
  });


  describe('#scan()', () => {
    it('should eventually succeed if valid binary is invoked', () => {
      const iosdeploy = new IosDeploy({ iosdeployBinary: './test/ios-deploy-dummy.sh' });

      const params = { timeout: 1 };
      const promise = iosdeploy.scan(params);

      return expect(promise).to.eventually.be.an('array').with.length(10);
    });

    it('should emit deviceFound events', async () => {
      const iosdeploy = new IosDeploy({ iosdeployBinary: './test/ios-deploy-dummy.sh' });

      const foundSpy = sinon.spy();
      iosdeploy.on('deviceFound', foundSpy);

      await iosdeploy.scan();

      expect(foundSpy).to.have.callCount(10);
      expect(foundSpy).to.always.have.been.calledWithMatch({
        name: sinon.match.string,
        uuid: sinon.match.string,
        port: sinon.match.in(['wifi', 'usb']),
      });
    });

    it('should fail if invalid binary is invoked', () => {
      const iosdeploy = new IosDeploy({ iosdeployBinary: 'non-existing-binary' });
      return expect(iosdeploy.scan()).to.eventually.be.rejected;
    });
  });
});
