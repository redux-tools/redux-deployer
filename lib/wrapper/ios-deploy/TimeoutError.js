export default class TimeoutError extends Error {

  /**
   * @param {?string} message
   */
  constructor(message = null) {
    super(message || 'Timed out waiting for device');
  }

}
