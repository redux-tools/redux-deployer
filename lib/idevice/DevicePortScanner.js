import Device from '../models/Device.js';
import IosDeploy from '../wrapper/ios-deploy/IosDeploy.js';

/**
 * Scan for connected iDevices and updates the Device objects in the given DeviceList
 * with the correct port information (Wifi or USB). If a device is found on both,
 * USB takes precedence.
 */
export default class DevicePortScanner {

  /**
   * @type {import('../models/DeviceList').default}
   */
  #deviceList;

  /** @type {typeof IosDeploy} */
  #iosdeployClass;

  /** @type {number} */
  #timeout;

  /**
   * @param {import('../models/DeviceList').default} deviceList
   * @param {Object} options
   * @param {typeof IosDeploy} options.iosdeployClass
   * @param {number} options.timeout
   */
  constructor(deviceList, { iosdeployClass = IosDeploy, timeout = 30 } = {}) {
    this.#deviceList = deviceList;
    this.#iosdeployClass = iosdeployClass;
    this.#timeout = timeout;
  }

  /**
   * Starts the scanner. The AbortController allows to stop the scanner before the ios-deploy
   * timeout
   * @param {AbortController} abort
   * @returns {Promise<boolean>} Resolves with success state. Never rejects!
   */
  async run(abort = new AbortController()) {
    // The AbortController's signal must not be aborted
    if (abort.signal.aborted) return this.#allUpdated();

    const iosdeploy = new this.#iosdeployClass();

    /**
     * Listener for the `deviceFound` event of `iosdeploy.scan`
     * @param {Object} Object
     * @param {string} Object.name
     * @param {string} Object.port
     */
    const deviceFound = ({ name: key, port }) => {
      const device = this.#deviceList.get(key);
      // Update devices with unknown port or wifi port (as we prefer USB over Wifi)
      if (device && (!device.port || device.port === Device.PORT_WIFI)) {
        device.port = port;
      }
      // Stop run if all devices are covered
      if (this.#allUpdated()) {
        abort.abort();
      }
    };
    iosdeploy.on('deviceFound', deviceFound);

    try {
      await iosdeploy.scan({ abortSignal: abort.signal, timeout: this.#timeout });
    } catch (e) {
      // do nothing
    }

    // Make sure that scanner process is terminated
    abort.abort();

    // Scan run is successful if no devices are without port
    return this.#allUpdated();
  }

  /**
   * @returns {boolean} True if all devices have their port scanned
   */
  #allUpdated() {
    // All devices have their port = no device has a null port
    return !this.#deviceList.getPorts().null;
  }

}
