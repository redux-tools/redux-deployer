import EventEmitter from 'events';
import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import Device from '../models/Device.js';
import DeviceList from '../models/DeviceList.js';
import DevicePortScanner from './DevicePortScanner.js';
import IosDeploy from '../wrapper/ios-deploy/IosDeploy.js';

use(chaiAsPromised);
use(sinonChai);

describe('DevicePortScanner', () => {

  /** @type {sinon.SinonSpy|typeof IosDeploy} */
  let IosDeployClassStub;
  /** @type {IosDeploy} */
  let iosDeployStub;
  /** @type {Device} */
  let deviceA;
  /** @type {Device} */
  let deviceB;
  /** @type {Device} */
  let deviceC;
  /** @type {DeviceList} */
  let deviceList;

  beforeEach(() => {
    // Setup a few Devices in a DeviceList
    deviceA = new Device('a');
    deviceB = new Device('b');
    deviceC = new Device('c');
    deviceList = new DeviceList().set('a', deviceA).set('b', deviceB).set('c', deviceC);

    iosDeployStub = new IosDeploy();
    sinon.stub(iosDeployStub, 'scan');
    IosDeployClassStub = sinon.spy(() => iosDeployStub);
  });

  afterEach(() => {
    sinon.restore();
  });

  describe('#run', () => {
    describe('on all devices with unknown port', () => {
      it('should return true and update all devices with port information', async () => {
        iosDeployStub.scan.callsFake(() => {
          iosDeployStub.emit('deviceFound', { name: 'a', port: 'wifi' });
          iosDeployStub.emit('deviceFound', { name: 'a', port: 'usb' }); // same device with 'usb' should override previous 'wifi'
          iosDeployStub.emit('deviceFound', { name: 'b', port: 'usb' });
          iosDeployStub.emit('deviceFound', { name: 'b', port: 'wifi' }); // same device with 'wifi' should NOT override previous 'usb'
          iosDeployStub.emit('deviceFound', { name: 'x', port: 'wifi' }); // unknown device should not have an influence
          iosDeployStub.emit('deviceFound', { name: 'c', port: 'wifi' });
        });

        const scanner = new DevicePortScanner(deviceList, { iosdeployClass: IosDeployClassStub });
        const result = await scanner.run();

        expect(result).to.be.true;
        expect(deviceA.port).to.be.equal('usb');
        expect(deviceB.port).to.be.equal('usb');
        expect(deviceC.port).to.be.equal('wifi');
      });

      it('should ignore if scan fails with exception', async () => {
        iosDeployStub.scan.callsFake(() => {
          iosDeployStub.emit('deviceFound', { name: 'a', port: 'wifi' });
          iosDeployStub.emit('deviceFound', { name: 'a', port: 'usb' }); // same device with 'usb' should override previous 'wifi'
          iosDeployStub.emit('deviceFound', { name: 'b', port: 'usb' });
          iosDeployStub.emit('deviceFound', { name: 'b', port: 'wifi' }); // same device with 'wifi' should NOT override previous 'usb'
          iosDeployStub.emit('deviceFound', { name: 'x', port: 'wifi' }); // unknown device should not have an influence
          iosDeployStub.emit('deviceFound', { name: 'c', port: 'wifi' });
          throw new Error();
        });

        const scanner = new DevicePortScanner(deviceList, { iosdeployClass: IosDeployClassStub });
        const result = await scanner.run();

        expect(result).to.be.true;
        expect(deviceA.port).to.be.equal('usb');
        expect(deviceB.port).to.be.equal('usb');
        expect(deviceC.port).to.be.equal('wifi');
      });

      it('should return false if aborted before any device is scanned', async () => {
        const abort = new AbortController();
        iosDeployStub.scan.callsFake(() => {
          abort.abort();
        });

        const scanner = new DevicePortScanner(deviceList, { iosdeployClass: IosDeployClassStub });
        const result = await scanner.run(abort);

        expect(result).to.be.false;
        expect(deviceA.port).to.be.null;
        expect(deviceB.port).to.be.null;
        expect(deviceC.port).to.be.null;
      });

      it('should return false if already aborted before scanning', async () => {
        const abort = new AbortController();
        abort.abort();

        const scanner = new DevicePortScanner(deviceList, { iosdeployClass: IosDeployClassStub });
        const result = await scanner.run(abort);

        expect(result).to.be.false;
        expect(deviceA.port).to.be.null;
        expect(deviceB.port).to.be.null;
        expect(deviceC.port).to.be.null;
      });
    });

    describe('some devices with known port', () => {

      beforeEach(() => {
        deviceA.port = 'usb';
        deviceB.port = 'wifi';
      });

      it('should return false if aborted before any device is scanned', async () => {
        const abort = new AbortController();
        iosDeployStub.scan.callsFake(() => {
          abort.abort();
        });

        const scanner = new DevicePortScanner(deviceList, { iosdeployClass: IosDeployClassStub });
        const result = await scanner.run(abort);

        expect(result).to.be.false;
        expect(deviceA.port).to.be.equal('usb');
        expect(deviceB.port).to.be.equal('wifi');
        expect(deviceC.port).to.be.null;
      });
    });

    describe('all devices with known port', () => {

      beforeEach(() => {
        deviceA.port = 'usb';
        deviceB.port = 'wifi';
        deviceC.port = 'usb';
      });

      it('should return true if aborted before any device is scanned', async () => {
        const abort = new AbortController();
        iosDeployStub.scan.callsFake(() => {
          abort.abort();
        });

        const scanner = new DevicePortScanner(deviceList, { iosdeployClass: IosDeployClassStub });
        const result = await scanner.run(abort);

        expect(result).to.be.true;
        expect(deviceA.port).to.be.equal('usb');
        expect(deviceB.port).to.be.equal('wifi');
        expect(deviceC.port).to.be.equal('usb');
      });

      it('should return true if already aborted before scanning', async () => {
        const abort = new AbortController();
        abort.abort();

        const scanner = new DevicePortScanner(deviceList, { iosdeployClass: IosDeployClassStub });
        const result = await scanner.run(abort);

        expect(result).to.be.true;
        expect(deviceA.port).to.be.equal('usb');
        expect(deviceB.port).to.be.equal('wifi');
        expect(deviceC.port).to.be.equal('usb');
      });

    });
  });
});
