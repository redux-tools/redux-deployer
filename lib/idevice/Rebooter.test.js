import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import Rebooter from './Rebooter.js';

use(chaiAsPromised);

describe('Rebooter', () => {
  describe('#reboot()', () => {
    it('should succeed if valid binary is invoked', () => {
      const rebooter = new Rebooter({ idevicediagnosticsBinary: './test/idevicediagnostics-dummy.sh' });
      return expect(rebooter.reboot('abc')).to.eventually.be.deep.equal({ uuid: 'abc' });
    });

    it('should fail if invalid binary is invoked', () => {
      const rebooter = new Rebooter({ idevicediagnosticsBinary: 'non-existing-binary' });
      return expect(rebooter.reboot('abc')).to.eventually.be.rejected;
    });
  });
});
