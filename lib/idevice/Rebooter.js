import { join } from 'path';
import { spawn } from 'child_process';
import ProgressMeter from '../util/ProgressMeter.js';

const IDEVICEDIAGNOSTICS_BINARY = join('/usr', 'local', 'bin', 'idevicediagnostics');

class Rebooter {

  #binary;

  #network;

  constructor({ idevicediagnosticsBinary = IDEVICEDIAGNOSTICS_BINARY, network = false } = {}) {
    this.#binary = idevicediagnosticsBinary;
    this.#network = network;
  }

  /**
   * @param {string} uuid Device ID
   * @param {ProgressMeter} meter
   * @returns {Promise<object>}
   */
  reboot(uuid, meter = new ProgressMeter()) {
    return new Promise((resolve, reject) => {
      const msg = (kv = {}) => ({ uuid, ...kv });

      const idevicediagnosticsParams = ['restart', '--udid', uuid];
      if (this.#network) {
        idevicediagnosticsParams.push('--network');
      }
      const child = spawn(this.#binary, idevicediagnosticsParams);
      meter.set({ percentage: 0.1, message: 'Reboot' });

      /** @type {?string} */
      let errorReason = null;

      child.on('close', (code) => {
        if (code === 0) {
          resolve(msg());
        } else if (errorReason) {
          reject(errorReason);
        } else {
          reject(new Error(`${this.#binary} returned error code ${code}`));
        }
      });

      child.on('error', (err) => {
        reject(msg({ code: err.errno }));
      });

      child.stdout.on('data', (/** @type {Buffer} */ buffer) => {
        const output = buffer.toString();
        if (output.match(/^No device found with udid/)) {
          errorReason = '(Device not found)';
        } else if (output.match(/^Restarting device/)) {
          meter.set({ percentage: 1.0, message: 'Rebooting' });
        }
      });

      child.stderr.on('data', (/** @type {Buffer} */ buffer) => {
        const output = buffer.toString();
        meter.error(output);
      });
    });
  }

}

export default Rebooter;
