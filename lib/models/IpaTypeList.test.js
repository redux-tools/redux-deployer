import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import IpaTypeList from './IpaTypeList.js';
import IpaType from './IpaType.js';

use(chaiAsPromised);

const CONFIG_TEST = [
  {
    id: 'flowerpot',
    friendlyName: 'Flower Pot',
    appId: 'local.panton.verner.flowerpot',
    latestIpa: {
      name: 'FlowerPot-v1.4',
      version: '1.4',
      url: 'http://localhost:5000/artefacts/FlowerPot.ipa',
    },
    configFiles: [
      { src: 'config/Setup.plist', dest: 'Setup.plist' },
    ],
  },
  {
    id: 'chair',
    friendlyName: 'The Chair',
    appId: 'local.panton.verner.chair',
    latestIpa: {
      name: 'Chair-R7',
      version: '7',
      url: 'http://localhost:5000/artefacts/Chair.ipa',
    },
  },
];

describe('IpaTypeList', () => {
  describe('#loadFromConfig', () => {
    describe('called with a valid array', () => {
      /** @type {IpaTypeList} */
      let list;

      beforeEach(async () => {
        list = await IpaTypeList.loadFromConfig(CONFIG_TEST);
      });

      it('should return a IpaTypeList (which is a Map)', () => {
        expect(list).to.be.instanceof(IpaTypeList);
        expect(list).to.be.instanceof(Map);
      });

      it('should return a Map with 2 properly keyed elements', () => {
        expect(list).to.have.lengthOf(2).and.have.all.keys('flowerpot', 'chair');
      });

      it('should return a Map with all elements of type IpaType', () => {
        expect(list.get('flowerpot')).to.be.instanceof(IpaType);
      });

      it('should return a Map with properly parsed IpaType instances', async () => {
        expect(list.get('flowerpot').id).to.equal('flowerpot');
        expect(list.get('flowerpot').friendlyName).to.equal('Flower Pot');
      });
    });
  });

  describe('called with non-array', async () => {
    it('should fail', async () => {
      const testObject = {};
      return expect(IpaTypeList.loadFromConfig(testObject)).to.be.rejectedWith('Config not an array');
    });
  });

  describe('#filter', () => {
    /** @type {IpaTypeList} */
    let list;

    beforeEach(async () => {
      list = await IpaTypeList.loadFromConfig(CONFIG_TEST);
    });

    it('should return the same list when called without filter', async () => {
      const filteredList = list.filter();
      expect(filteredList).to.be.instanceof(IpaTypeList);
      expect(filteredList).to.be.equal(list);
    });
    it('should return a filtered list when called with array of keys', async () => {
      const filteredList = list.filter(['chair']);
      expect(filteredList).to.be.instanceof(IpaTypeList);
      expect(filteredList).to.have.lengthOf(1).and.have.all.keys('chair');
    });
    it('should return a filtered list when called with a function', async () => {
      const filteredList = list.filter(([_key, ipatype]) => ipatype.friendlyName.match(/^Flow/));
      expect(filteredList).to.be.instanceof(IpaTypeList);
      expect(filteredList).to.have.lengthOf(1).and.have.all.keys('flowerpot');
    });
    it('should throw if called with invalid filter', async () => {
      expect(() => list.filter('no strings attached')).to.throw();
    });
  });

  describe('#map', () => {
    /** @type {IpaTypeList} */
    let list;

    beforeEach(async () => {
      list = await IpaTypeList.loadFromConfig(CONFIG_TEST);
    });

    it('should return an array with mapped elements', () => {
      const map = list.map((ipaType) => ipaType.friendlyName === 'Flower Pot');
      expect(map).to.be.an('array').with.lengthOf(2);
      expect(map[0]).to.be.a('boolean').equals(true);
      expect(map[1]).to.be.a('boolean').equals(false);
    });

    it('should return an array with the list elements when called without function', () => {
      const map = list.map();
      expect(map).to.be.an('array').with.lengthOf(2);
      expect(map[0]).to.be.equal(list.get('flowerpot'));
      expect(map[1]).to.be.equal(list.get('chair'));
    });
  });

});
