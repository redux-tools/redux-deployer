import { expect } from 'chai';
import Device from './Device.js';

describe('Device', () => {
  describe('#getters', () => {
    it('should return uuid, name, port and type', () => {
      const device = new Device('abc', { name: 'def', port: 'ghi', type: 'jkl' });
      expect(device.uuid).to.be.equal('abc');
      expect(device.name).to.be.equal('def');
      expect(device.port).to.be.equal('ghi');
      expect(device.type).to.be.equal('jkl');
    });

    it('should return uuid as name if none is given', () => {
      const device = new Device('abc');
      expect(device.uuid).to.be.equal('abc');
      expect(device.name).to.be.equal('abc');
      expect(device.port).to.be.null;
      expect(device.type).to.be.null;
    });
  });

  describe('#set port', () => {
    it('should set the port', () => {
      const device = new Device('abc');
      device.port = Device.PORT_USB;
      expect(device.port).to.be.equal(Device.PORT_USB);
    });

    it('should emit an update event', (done) => {
      const device = new Device('abc');
      device.on('update', (d) => {
        expect(d).to.be.equal(device);
        expect(d.port).to.be.equal(Device.PORT_WIFI);
        done();
      });
      device.port = Device.PORT_WIFI;
    });

    it('should fail if port set to unknown value', () => {
      const device = new Device('abc');
      expect(() => { device.port = 'def'; }).to.throw(Error);
    });
  });
});
