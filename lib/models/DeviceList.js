import Device from './Device.js';

export default class DeviceList extends Map {

  /**
   * Returns a populated device list from the given string (text or json)
   * @param {string} string
   * @param {Object} options
   * @param {boolean} options.forceJson
   * @returns {Promise<DeviceList>}
   * @throws
   */
  static async loadFromString(string, { forceJson = false } = {}) {
    let devices;
    try {
      const json = JSON.parse(string);
      devices = this.#fromJson(json);
    } catch (e) {
      if (forceJson) throw e;
      devices = this.#fromText(string);
    }
    return new DeviceList(devices.map((device) => [device.name, device]));
  }

  /**
   * Returns a new DeviceList filtered by the given device keys
   * @param {(function([string,any]):any)|string[]|null} filter Filter (either function or list of
   *                                                            device keys, default: no filter)
   * @returns {DeviceList}
   */
  filter(filter = null) {
    if (filter === null) return this;

    let filterFn;
    if (Array.isArray(filter)) {
      /** @param {[string,any]} array */
      filterFn = ([key, _]) => filter.indexOf(key) >= 0;
    } else if (typeof filter === 'function') {
      filterFn = filter;
    } else {
      throw new Error(`Unknown filter: ${filter}`);
    }

    return new DeviceList([...this].filter(filterFn));
  }

  /**
   * Applies the given callback function to the device list and returns an array
   * @param {function(Device,string):any} fn
   * @returns {Array<any>}
   */
  map(fn = (device) => device) {
    return [...this].map(([key, device]) => fn(device, key));
  }

  /**
   * Returns a hash with the number of devices for each IPA type
   * @returns {Object.<string,number>}
   */
  getTypes() {
    /** @type {Object.<string,number>} */
    const types = {};
    this.forEach((device) => { types[device.type] = (types[device.type] || 0) + 1; });
    return types;
  }

  /**
   * Returns an array with the unique IPA types configured for devices in the list
   * @returns {string[]}
   */
  getIpaTypeIds() {
    return Object.keys(this.getTypes());
  }

  /**
   * Returns a hash with the number of devices for each port (usb, wifi)
   * @returns {Object.<string,number>}
   */
  getPorts() {
    /** @type {Object.<string,number>} */
    const ports = {};
    this.forEach((device) => { ports[device.port] = (ports[device.port] || 0) + 1; });
    return ports;
  }

  /**
   * @param {string} text
   * @returns {Device[]}
   */
  static #fromText(text) {
    try {
      return (
        text
          .split(/\r?\n/)
          .filter((line) => !line.match(/^(#|\s*$)/))
          .map((line) => line.match(/([^:]+):([0-9a-f]{40}|[0-9a-f]{8}-[0-9a-f]{16}):(\w+)\s*$/i))
          .map(([_, name, uuid, type]) => new Device(uuid, { name, type }))
      );
    } catch (e) {
      if (e instanceof TypeError) {
        throw new Error('Error parsing device list');
      }
      throw e;
    }
  }

  /**
   * @param {string} json
   * @returns {Device[]}
   * @throws
   */
  static #fromJson(json) {
    if (!Array.isArray(json)) throw new Error('JSON not an array');
    return (
      json
        .map(({ name, uuid, type }) => new Device(uuid, { name, type }))
    );
  }

}
