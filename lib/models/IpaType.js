import { createActionsFromConfig } from '../configurator/ActionFactory.js';

/**
 * @typedef {Object} IpaArtifact
 * @property {string} name
 * @property {string} version
 * @property {string} url
 */

/**
 * Defines a specific type of IPA
 */
export default class IpaType {

  /** @type {string} */
  #id;

  /** @type {string} */
  #friendlyName;

  /** @type {string} */
  #appId;

  /** @type {IpaArtifact} */
  #latestIpa;

  /** @type {import('../configurator/Action').default[]} */
  #config;

  /**
   * @param {string} id
   * @param {Object} data
   * @param {string} data.friendlyName
   * @param {string} data.appId
   * @param {IpaArtifact} data.latestIpa
   * @param {import('../configurator/Action').default[]} data.config
   */
  constructor(
    id,
    {
      friendlyName = null,
      appId = null,
      latestIpa,
      config = [],
    } = {},
  ) {
    this.#id = id;
    this.#friendlyName = friendlyName || id;
    this.#appId = appId;
    this.#latestIpa = latestIpa;
    this.#config = createActionsFromConfig(config);
  }

  /**
   * @returns {string}
   */
  get id() {
    return this.#id;
  }

  /**
   * @returns {string}
   */
  get friendlyName() {
    return this.#friendlyName;
  }

  /**
   * @returns {string}
   */
  get appId() {
    return this.#appId;
  }

  /**
   * @returns {?IpaArtifact}
   */
  get latestIpa() {
    if (this.#latestIpa === undefined) return null;
    return { ...this.#latestIpa }; // Return shallow copy
  }

  /**
   * This list of IpaConfiguratorAction is used in the IpaConfigurator to configure
   * an IPA which is usually done in IpaSigner prior to the signing stage.
   * @returns {import('../configurator/Action').default[]}
   */
  get config() {
    return this.#config;
  }

}
