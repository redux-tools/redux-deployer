import { expect } from 'chai';
import IpaType from './IpaType.js';
import IpaConfiguratorAction from '../configurator/Action.js';

describe('IpaType', () => {
  describe('constructor', () => {
    it('should throw if config.action is not set properly', () => {
      const config = [{ action: 'invalid' }];
      expect(() => new IpaType('abc', { config })).to.throw();
    });
  });

  describe('#getters', () => {
    it('should return id, friendlyName and appId', () => {
      const device = new IpaType('abc', { friendlyName: 'def', appId: 'ghi' });
      expect(device.id).to.be.equal('abc');
      expect(device.friendlyName).to.be.equal('def');
      expect(device.appId).to.be.equal('ghi');
    });

    it('should return id as friendlyName if none is given', () => {
      const device = new IpaType('abc');
      expect(device.id).to.be.equal('abc');
      expect(device.friendlyName).to.be.equal('abc');
    });

    it('should return latestIpa as shallow clone', () => {
      const latestIpa = { name: 'def', version: '1.2.3' };
      const device = new IpaType('abc', { latestIpa });
      expect(device.latestIpa).to.be.not.equal(latestIpa); // not equal because it's a clone
      expect(device.latestIpa).to.be.deep.equal(latestIpa); // deep equal because it's a clone
    });

    it('should return config as array of IpaConfiguratorAction', () => {
      const config = [{ action: 'skip' }];
      const device = new IpaType('abc', { config });
      expect(device.config).to.be.an('array').with.length(1);
      expect(device.config[0]).to.be.instanceOf(IpaConfiguratorAction);
    });
  });
});
