import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import Action from './Action.js';

use(chaiAsPromised);

describe('configurator/Action', () => {
  describe('ignoreError', () => {
    it('should return true', () => {
      const action = new Action({ ignoreErrors: true });
      expect(action.ignoreErrors).to.be.true;
    });

    it('should return false', () => {
      const action = new Action({ ignoreErrors: false });
      expect(action.ignoreErrors).to.be.false;
    });

    it('should default to false', () => {
      const action = new Action({});
      expect(action.ignoreErrors).to.be.false;
    });
  });

  describe('#run', () => {
    it('should do nothing', () => {
      const action = new Action({});
      return expect(action.run()).to.be.fulfilled;
    });
  });
  describe('#toString', () => {
    it('should return "Do nothing"', () => {
      const action = new Action({});
      expect(action.toString()).to.equal('Do nothing');
    });
  });
});
