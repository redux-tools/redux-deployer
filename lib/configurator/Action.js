export default class Action {

  /** @type {boolean} */
  #ignoreErrors;

  /**
   * @param {Object} config
   * @param {boolean} config.ignoreErrors
   */
  constructor({ ignoreErrors = false }) {
    this.#ignoreErrors = !!ignoreErrors;
  }

  /**
   * @returns {boolean}
   */
  get ignoreErrors() {
    return this.#ignoreErrors;
  }

  /**
   * @param {string} _ipaDir
   * @param {string} _configBaseDir
   */
  // eslint-disable-next-line class-methods-use-this
  async run(_ipaDir, _configBaseDir) {
    // do nothing
  }

  /**
   * @returns {string}
   */
  // eslint-disable-next-line class-methods-use-this
  toString() {
    return 'Do nothing';
  }

}
