import { join, normalize } from 'path';
import { promisify } from 'util';
import { execFile as execFileCb } from 'child_process';
import Action from './Action.js';

const execFile = promisify(execFileCb);

const PLISTBUDDY_BINARY = join('/usr', 'libexec', 'PlistBuddy');

/**
 * Configurator Action to modify a PList file via PlistBuddy
 */
export default class PlistAction extends Action {

  /** @type {string} */
  #file;

  /** @type {string} */
  #command;

  /** @type {string} */
  #property;

  /** @type {string} */
  #value;

  /** @type {string} */
  #type;

  /** @type {string} */
  #plistbuddyBinary;

  /**
   * @param {Object} config
   * @param {string} config.file Path to PList file
   * @param {string} config.command
   * @param {string} config.property
   * @param {string} config.value
   * @param {string} config.type
   * @param {Object} options
   * @param {string} options.plistbuddyBinary
   */
  constructor(
    {
      file,
      command,
      property,
      value = '',
      type = '',
      ...config
    },
    {
      plistbuddyBinary = PLISTBUDDY_BINARY,
    } = {},
  ) {
    super(config);

    // Since we're dealing with local file locations, but this class could be
    // created from configuration fetched from a remote resource, we apply some
    // extra care.
    if (typeof file !== 'string' || file.length === 0) {
      throw new Error(`File path invalid: ${file}`);
    }
    if (normalize(file).match(/^(\.\.(\/|\\|$))+/)) {
      throw new Error(`File path contains directory traversal: ${file}`);
    }

    this.#file = file;
    this.#command = command;
    this.#property = property;
    this.#value = value;
    this.#type = type;
    this.#plistbuddyBinary = plistbuddyBinary;
  }

  /**
   * @param {string} ipaDir
   */
  async run(ipaDir) {
    const plist = join(ipaDir, this.#file);
    await this.#plistbuddy(plist, this.#command, this.#property, this.#value, this.#type);
  }

  /**
   * @returns {string}
   */
  toString() {
    return `Modified ${this.#file}: ${this.#command} ${this.#property}`;
  }

  /**
   * Executes a plistbuddy command on file
   * @param {string} file Path to PList file
   * @param {string} command
   * @param {string} property
   * @param {string} value
   * @param {string} type
   * @returns {Promise<string>}
   */
  async #plistbuddy(file, command, property, value = '', type = '') {
    const args = ['-x', '-c', `${command} ${property} ${type} ${value}`.trim(), file];
    try {
      const { stdout } = await execFile(this.#plistbuddyBinary, args);
      return stdout;
    } catch (/** @type {any} */ e) {
      if (e.code !== 1) {
        throw e;
      }
      if (e.stderr.match(/Delete: Entry, .*? Does Not Exist/)) {
        throw new Error('Entry does not exist');
      }
      if (e.stderr.match(/Add: .*? Entry Already Exists/)) {
        throw new Error('Entry already exists');
      }
      throw new Error(e.stderr.replace(/\n/g, ''));
    }
  }

}
