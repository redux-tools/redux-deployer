import PlistAction from './PlistAction.js';

export default class VersionAction extends PlistAction {

  #version;

  /**
   * @param {Object} config
   * @param {string} config.version
   * @param {string} config.file
   */
  constructor({ version, file = 'Info.plist', ...config }) {
    super({
      file,
      command: 'set',
      property: ':CFBundleShortVersionString',
      value: version,
      ...config,
    });
    this.#version = version;
  }

  /**
   * @returns {string}
   */
  toString() {
    return `Set version to ${this.#version}`;
  }

}
