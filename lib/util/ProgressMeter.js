import { EventEmitter } from 'events';

function round(value) {
  return Math.round(value * 1e4) / 1e4;
}

export default class ProgressMeter extends EventEmitter {

  /** @type {string} */
  #name;

  /** @type {number} */
  #initial;

  /** @type {number} */
  #current;

  /** @type {number} */
  #from;

  /** @type {number} */
  #to;

  /**
   * @param {Object} params
   * @param {string} params.name
   * @param {number} params.initial
   * @param {string} params.name
   * @param {number[]} params.scale
   * @param {?import('../ui/ProgressBar').default} params.progressBar Optional ProgressBar which
   *        will be connected to this meter automatically
   */
  constructor({
    name = '',
    initial = 0.0,
    scale: [scaleMin, scaleMax] = [0.0, 1.0],
    progressBar = null,
  } = {}) {
    super();
    this.#name = name;
    this.#initial = initial;
    this.#current = initial;
    this.#from = scaleMin;
    this.#to = scaleMax;
    if (progressBar) {
      this.on('progress', (params) => progressBar.update(params));
      this.on('error', ({ message, error }) => progressBar.error(message, error));
    }
  }

  /**
   * @param {number|{percentage:number,message:string,log:boolean}} percentage
   * @param {string} message
   * @param {?boolean} log
   * @param {?object} options
   */
  set(percentage, message = '', log = null, options = {}) {
    if (typeof percentage === 'object') {
      message = percentage.message || '';
      log = (typeof percentage.log === 'undefined') ? null : percentage.log;
      options = { ...percentage };
      percentage = percentage.percentage;
    }
    if (percentage > 1.0) percentage = 1.0;
    if (percentage < this.#current) throw Error('No way back');
    this.#current = percentage;
    this.#emitProgress(message, log, options);
  }

  /**
   * @param {string} message
   * @param {?Error} error
   */
  error(message, error = null) {
    this.emit('error', { message, error });
  }

  /**
   * Emit a retry event and reset this meter's scaling.
   * The scaling's new lower boundary is set so that the progress starts from its current location
   * or that it snaps back to 50% if it the current progress is already above 50%.
   * @param {string} message
   * @param {?Error} error
   * @param {?boolean} log
   * @param {?object} options
   */
  retry(message, error, log = null, options = {}) {
    const fromDelta = (this.#to - this.#from) * Math.min(this.#current, 0.5);
    this.#from = round(this.#from + fromDelta);
    this.#current = this.#initial;
    this.emit('retry', { message, error });
    this.#emitProgress(message, log, options);
  }

  /**
   * @param {number} maxPercentage
   * @param {string} prefix
   * @return {ProgressMeter}
   */
  partial(maxPercentage = 1.0, prefix = '') {
    if (maxPercentage <= this.#current) throw Error('No way back');
    const meter = new ProgressMeter({
      name: `${prefix} ${this.#name}`.trim(),
      initial: 0.0,
      scale: [this.#current, maxPercentage],
    });
    meter.on('progress', (params) => this.set(params));
    return meter;
  }

  /**
   * @param {string} message
   * @param {?boolean} log
   * @param {?object} options
   */
  #emitProgress(message, log, options) {
    this.emit('progress', {
      ...options,
      name: this.#name,
      percentage: round(this.#from + (this.#to - this.#from) * this.#current),
      message,
      log,
    });
  }

}
