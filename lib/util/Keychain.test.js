import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import Keychain from './Keychain.js';

use(chaiAsPromised);

describe('Keychain', () => {
  describe('#constructor()', () => {
    it('should default to "security" binary', () => {
      const keychain = new Keychain();
      return expect(keychain.checkExecutable()).to.be.fulfilled;
    });
  });

  describe('#unlockKeychain()', () => {
    it('should resolve to have the default keychain unlocked', async () => {
      const keychain = new Keychain({ securityBinary: './test/security-dummy.sh' });
      return expect(keychain.unlockKeychain('abc')).to.eventually.fulfilled;
    });

    it('should resolve to have a specific keychain unlocked', async () => {
      const keychain = new Keychain({ securityBinary: './test/security-dummy.sh' });
      return expect(keychain.unlockKeychain('abc', 'def.keychain')).to.eventually.fulfilled;
    });
  });

  describe('#getDefaultKeychain()', () => {
    it('should resolve to get the default keychain', async () => {
      const keychain = new Keychain({ securityBinary: './test/security-dummy.sh' });
      return expect(keychain.getDefaultKeychain()).to.eventually.fulfilled;
    });
  });
});
