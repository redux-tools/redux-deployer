import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import mockfs from 'mock-fs';
import IpaFinder from './IpaFinder.js';

use(chaiAsPromised);

describe('IpaFinder', () => {
  describe('#getNewestVersion', () => {
    const assertions = [
      {
        result: 'test-4.11.txt',
        pattern: '*.txt',
        files: [
          'test-3.txt',
          'test-4.11.txt',
          'test-4.0.1.txt',
          'test-4.10.1.txt',
        ],
      },
      {
        result: '/path2/testv2.01.ipa',
        pattern: '/**/*.ipa',
        files: [
          '/path2/testv1.01.ipa',
          '/path2/testv1.11.ipa',
          '/path2/testv2.01.ipa',
          '/path2/testv1.02.ipa',
        ],
      },
      {
        result: '/path3/test3-7.4.ipa',
        pattern: '/path3/*(?<!debug)\\.ipa',
        files: [
          '/path3/test3-7.4.ipa',
          '/path3/test3-7.4-debug.ipa',
          '/path3/test3-7.3.ipa',
          '/path3/test2-7.4.ipa',
        ],
      },
      {
        result: false,
        pattern: '/**/*.txt',
        files: [
          '/path4/test3-7.4.ipa',
          '/path4/test3-7.4-debug.ipa',
          '/path4/test3-7.3.ipa',
          '/path4/test2-7.4.ipa',
        ],
      },
    ];

    afterEach(() => {
      mockfs.restore();
    });

    assertions.forEach(({ result, pattern, files }, index) => {
      it(`should fulfill assertion ${index + 1} (${result})`, async () => {
        mockfs(Object.fromEntries(files.map((file) => [file, 'mock'])));
        const finder = new IpaFinder();
        return expect(finder.getNewestVersion(pattern)).to.eventually.be.equal(result);
      });
    });
  });
});
