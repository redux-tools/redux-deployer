/* eslint-disable class-methods-use-this */

import fastglob from 'fast-glob';

export default class IpaFinder {

  /**
   * Returns most recent file of specified type.."whatever.ipa"
   * @param {string} pattern Relative path match pattern (micromatch glob pattern)
   * @returns {Promise<string|false>} Relative path or false if nothing found
   */
  async getNewestVersion(pattern) {
    const files = await fastglob(pattern);
    files.sort((a, b) => b.localeCompare(a, 'en', { numeric: true }));
    return files[0] || false;
  }

}
