import { expect, use } from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import ProgressMeter from './ProgressMeter.js';

use(sinonChai);

describe('ProgressMeter', () => {
  describe('constructor', () => {
    it('should wire a ProgressBar when passed', () => {
      const pbDummy = { update: sinon.spy(), error: sinon.spy() };

      const meter = new ProgressMeter({ progressBar: pbDummy });
      meter.set(0.5);
      meter.error('Error');

      expect(pbDummy.update).to.have.been.calledOnce;
      expect(pbDummy.error).to.have.been.calledOnce;
    });
  });

  describe('when constructed without scaling', () => {
    /** @type {ProgressMeter} */
    let meter;
    let spy;
    beforeEach(() => {
      meter = new ProgressMeter();
      spy = sinon.spy();
      meter.on('progress', spy);
    });

    describe('#set', () => {
      it('should emit progress events with proper percentages', () => {
        meter.set(0.5);
        meter.set(1.0);
        expect(spy).to.have.been.calledTwice;
        expect(spy.args[0][0]).to.include({ percentage: 0.5 });
        expect(spy.args[1][0]).to.include({ percentage: 1.0 });
      });

      it('should clamp progress on 100%', () => {
        meter.set({ percentage: 0.5 }); // <- use alternative syntax here
        meter.set({ percentage: 1.1 });
        expect(spy.args[1][0]).to.include({ percentage: 1.0 });
      });

      it('should throw if progress is decreased', () => {
        meter.set(0.7);
        expect(() => meter.set(0.5)).to.throw(/^No way back/);
      });
    });

    describe('#retry', () => {
      it('should restart with new scaling from 50% progress (if progress was already beyond 50%)', () => {
        meter.set(0.75);
        meter.retry('Retry');
        meter.set(0.4);
        expect(spy).to.have.been.calledThrice;
        expect(spy.args[0][0]).to.include({ percentage: 0.75 });
        expect(spy.args[1][0]).to.include({ percentage: 0.5 });
        expect(spy.args[2][0]).to.include({ percentage: 0.7 });
      });

      it('should restart with new scaling from current progress (if progress was less than 50%)', () => {
        meter.set(0.2);
        meter.retry('Retry');
        meter.set(0.5);
        expect(spy).to.have.been.calledThrice;
        expect(spy.args[0][0]).to.include({ percentage: 0.2 });
        expect(spy.args[1][0]).to.include({ percentage: 0.2 });
        expect(spy.args[2][0]).to.include({ percentage: 0.6 });
      });
    });
  });

  describe('when constructed with scaling', () => {
    /** @type {ProgressMeter} */
    let meter;
    let spy;
    beforeEach(() => {
      meter = new ProgressMeter({ scale: [0.2, 0.6] });
      spy = sinon.spy();
      meter.on('progress', spy);
    });

    describe('#set', () => {
      it('should emit progress events with scaled percentages', () => {
        meter.set(0.5);
        meter.set(1.0);
        expect(spy).to.have.been.calledTwice;
        expect(spy.args[0][0]).to.include({ percentage: 0.4 });
        expect(spy.args[1][0]).to.include({ percentage: 0.6 });
      });

      it('should clamp progress on 100% (scaled to 60%)', () => {
        meter.set(0.5);
        meter.set(1.1);
        expect(spy.args[1][0]).to.include({ percentage: 0.6 });
      });

      it('should throw if progress is decreased', () => {
        meter.set(0.7);
        expect(() => meter.set(0.5)).to.throw(/^No way back/);
      });
    });

    describe('#retry', () => {
      it('should restart with new scaling from 50% progress (if progress was already beyond 50%)', () => {
        meter.set(0.75);
        meter.retry('Retry');
        meter.set(0.4);
        expect(spy).to.have.been.calledThrice;
        expect(spy.args[0][0]).to.include({ percentage: 0.5 });
        expect(spy.args[1][0]).to.include({ percentage: 0.4 });
        expect(spy.args[2][0]).to.include({ percentage: 0.48 });
      });

      it('should restart with new scaling from current progress (if progress was less than 50%)', () => {
        meter.set(0.25);
        meter.retry('Retry');
        meter.set(0.5);
        expect(spy).to.have.been.calledThrice;
        expect(spy.args[0][0]).to.include({ percentage: 0.3 });
        expect(spy.args[1][0]).to.include({ percentage: 0.3 });
        expect(spy.args[2][0]).to.include({ percentage: 0.45 });
      });
    });
  });

  describe('#error', () => {
    it('should emit an error event', () => {
      const meter = new ProgressMeter();
      const spy = sinon.spy();
      meter.on('error', spy);

      meter.error('abc');
      expect(spy).to.have.been.calledOnce;
      expect(spy.args[0][0]).to.include({ message: 'abc' });
    });
  });

  describe('#partial', () => {
    it('should return a new ProgressMeter', () => {
      const meter = new ProgressMeter();

      const partial = meter.partial(0.8);
      expect(partial).to.be.instanceOf(ProgressMeter);
      expect(partial).to.be.not.equal(meter);
    });

    it('should throw if partial would start with less progress', () => {
      const meter = new ProgressMeter();
      meter.set(0.8);

      expect(() => meter.partial(0.5)).to.throw(/^No way back/);
    });
  });
});
