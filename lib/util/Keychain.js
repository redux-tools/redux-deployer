import { promisify } from 'util';
import { join } from 'path';
import { execFile as execFileCb } from 'child_process';

const execFile = promisify(execFileCb);

const SECURITY_BINARY = join('/usr', 'bin', 'security');

class Keychain {

  /** @type {string} */
  #securityBinary;

  /**
   * @param {Object} options
   * @param {string} options.securityBinary
   */
  constructor({ securityBinary = SECURITY_BINARY } = {}) {
    this.#securityBinary = securityBinary;
  }

  async checkExecutable() {
    await execFile(this.#securityBinary, ['help']);
  }

  /**
   * @param {string} password
   * @param {?string} keychain
   */
  async unlockKeychain(password, keychain = null) {
    const usedKeychain = keychain || await this.getDefaultKeychain();
    await execFile(this.#securityBinary, ['unlock-keychain', '-p', password, usedKeychain]);
  }

  /**
   * Obtains the name of the user's default MacOS keychain
   * @returns {Promise<string>}
   */
  async getDefaultKeychain() {
    const { stdout } = await execFile(this.#securityBinary, ['default-keychain']);
    return stdout.trim().replace(/"/g, '');
  }

}

export default Keychain;
