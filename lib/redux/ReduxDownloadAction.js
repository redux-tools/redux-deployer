import { join, dirname } from 'path';
import { stat, mkdir } from 'fs/promises';
import pFilter from 'p-filter';
import pMap from 'p-map';
import { createDownloaderForUrl } from '../downloader/Factory.js';
import ProgressScreen from '../ui/ProgressScreen.js';
import { RELEASES_BASE_DIR } from './ReduxConfig.js';

/**
 * @typedef {Object} Task
 * @property {string} type
 * @property {?string} name
 * @property {string} url
 * @property {string} ipa
 * @property {boolean} refresh
 * @property {import('../ui/ProgressBar').default} bar
 */

/**
 * @param {import('../models/IpaType').default} ipaType
 * @returns {Task}
 */
function createTask(ipaType) {
  const { id, latestIpa } = ipaType;
  const { url, name } = latestIpa;
  return ({
    type: id,
    name,
    url,
    ipa: null,
    refresh: false,
    bar: null,
  });
}

/**
 * @param {Task[]} tasks
 * @param {ProgressScreen} progress
 * @returns {Task[]}
 */
function amendProgressBars(tasks, progress) {
  tasks.forEach((task) => {
    const { type } = task;
    task.bar = progress.bar({ item: type, logUpdates: true });
  });
  return tasks;
}

/**
 * @param {Task[]} tasks
 * @returns {Task[]}
 */
function amendIpaFilenames(tasks) {
  tasks.forEach((task) => {
    const { type, name } = task;
    task.ipa = join(RELEASES_BASE_DIR, type, `${name}.ipa`);
  });
  return tasks;
}

/**
 * @param {Task} task
 * @returns {Promise<boolean>}
 */
async function isTaskRequired(task) {
  const { ipa, bar, refresh } = task;
  const ipaExists = !!await stat(ipa).catch(() => false);
  if (ipaExists && refresh) {
    bar.log(`Download to update existing ${ipa}`);
    return true;
  }
  if (ipaExists) {
    bar.done('Latest IPA found locally, skipping download');
    return false;
  }
  bar.log(`Newer IPA available, downloading to ${ipa}`);
  return true;
}

/**
 * @param {Task} task
 * @throws
 */
async function downloadIpa(task) {
  const { url, ipa, bar } = task;

  /** @param {{percentage: number}} param */
  const progressCb = ({ percentage }) => bar.update(percentage, `Downloading to ${ipa}`, false);

  try {
    await mkdir(dirname(ipa), { recursive: true });
    const dl = createDownloaderForUrl(url);
    await dl.download(url, ipa, progressCb);
    bar.done('Downloading IPA finished');
  } catch (/** @type {any} */ e) {
    bar.fail(e);
    throw e;
  }
}

/**
 * Downloads the required IPAs. If `url` is specified, use it and download the content
 * to the release folder of `type` with the given `name`; otherwise, the latest IPAs of
 * the given `deviceTypes` are fetched and downloaded.
 * @param {Object} params
 * @param {import('./ReduxConfig').default} params.reduxConfig
 * @param {?string} params.url
 * @param {?string} params.type
 * @param {?string} params.name
 * @param {string[]} params.deviceTypes
 */
export default async function download({
  reduxConfig,
  url = null,
  type = null,
  name = null,
  deviceTypes = [],
}) {
  const { ipaTypeList } = reduxConfig;

  const progress = new ProgressScreen(type !== null ? [type] : deviceTypes, { title: 'Download IPAs' });

  // Build a task list, each item containing download URL, IPA type, IPA name and progress bar
  /** @type {Task[]} */
  let tasks = [];
  if (url !== null && type !== null) {
    tasks.push({
      type,
      name,
      url,
      ipa: null,
      refresh: true,
      bar: null,
    });
  } else {
    tasks = deviceTypes.map((ipaType) => createTask(ipaTypeList.get(ipaType)));
  }

  // Add ProgressBar and IPA filenames to each task
  tasks = amendProgressBars(tasks, progress);
  tasks = amendIpaFilenames(tasks);

  // Keep only tasks for non-existing IPA files
  tasks = await pFilter(tasks, isTaskRequired);

  // Download them
  try {
    await pMap(tasks, downloadIpa, { stopOnError: false });
  } finally {
    progress.close();
  }
}
