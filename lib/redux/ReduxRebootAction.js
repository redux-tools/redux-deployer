import sleep from 'sleep-promise';
import Device from '../models/Device.js';
import ProgressScreen from '../ui/ProgressScreen.js';
import Rebooter from '../idevice/Rebooter.js';
import ProgressMeter from '../util/ProgressMeter.js';

/**
 * Reboot the given device
 * @param {Object} params
 * @param {Device} params.device
 * @param {number} params.sleepTime
 * @param {import('../ui/ProgressBar').default} params.progressBar
 */
export async function rebootDevice({ device, sleepTime = 0, progressBar }) {
  const { uuid } = device;
  const network = (device.port === Device.PORT_WIFI);
  const meter = new ProgressMeter({ progressBar });

  const rebooter = new Rebooter({ network });

  try {
    await rebooter.reboot(uuid, meter);
    if (sleepTime > 0) {
      await sleep(sleepTime);
    }
    progressBar.done('Reboot complete');
  } catch (/** @type {any} */ e) {
    progressBar.fail('Reboot failed', e);
  }
}

/**
 * Reboots all devices in the given list
 * @param {Object} params
 * @param {import('../models/DeviceList').default} params.deviceList
 * @param {number} params.sleepTime
 */
export async function reboot({ deviceList, sleepTime = 0 }) {
  const progress = new ProgressScreen(deviceList, { title: 'Reboot devices' });
  await Promise.allSettled(
    deviceList.map((device) => rebootDevice({
      device,
      sleepTime,
      progressBar: progress.bar({ item: device }),
    })),
  );
  progress.close();
}
