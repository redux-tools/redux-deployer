import { expect } from 'chai';
import { formatDeployResult } from './ReduxDeployAction.js';

describe('ReduxDeployAction/formatDeployResult', () => {

  it('should return confirmation if all results are fine', () => {
    const input = {
      abc: true,
      ghi: true,
      def: true,
    };
    const result = formatDeployResult(input);
    return expect(result).to.be.equal('Deployment successful on all 3 devices.');
  });

  it('should return failure report with devices', () => {
    const input = {
      abc: false,
      def: true,
      ghi: false,
    };
    const result = formatDeployResult(input);
    return expect(result).to.be.equal('Deployment failed on abc, ghi.');
  });

  it('should return failure report with sorted devices', () => {
    const input = {
      abc: true,
      ghi: false,
      def: false,
    };
    const result = formatDeployResult(input);
    return expect(result).to.be.equal('Deployment failed on def, ghi.');
  });

});
