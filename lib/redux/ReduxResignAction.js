import { join, parse, format } from 'path';
import { stat } from 'fs/promises';
import pFilter from 'p-filter';
import IpaConfigurator from '../configurator/IpaConfigurator.js';
import IpaFinder from '../util/IpaFinder.js';
import IpaSigner from '../ipa/IpaSigner.js';
import Keychain from '../util/Keychain.js';
import ProgressScreen from '../ui/ProgressScreen.js';
import VersionAction from '../configurator/VersionAction.js';
import ProgressMeter from '../util/ProgressMeter.js';
import { CONFIG_BASE_DIR, RELEASES_BASE_DIR } from './ReduxConfig.js';

const ipaFinder = new IpaFinder();

/**
 * @typedef {Object} Task
 * @property {import('../models/IpaType').default} ipaType IPA type
 * @property {string} ipaBasePath
 * @property {string|false} ipaFile A packaged IPA (or false if none was found)
 * @property {string} signedIpaFile Target filename of the signed IPA
 * @property {string} provisioningProfile Path to the provisioning profile
 * @property {string} developerCertificate Name of the Apple Developer Certificate to use
 *                                         for signing
 * @property {?string} version If set, modify the App's Info.plist version
 * @property {import('../ui/ProgressBar').default} bar
 */

/**
 * @param {import('../models/IpaType').default} ipaType
 * @returns {Promise<Task>}
 */
async function createTask(ipaType) {
  const ipaBasePath = join(RELEASES_BASE_DIR, ipaType.id);
  const pattern = join(ipaBasePath, '*(?<!Signed)\\.ipa'); // Only unsigned IPAs
  const ipaFile = await ipaFinder.getNewestVersion(pattern);
  return ({
    ipaType,
    ipaBasePath,
    ipaFile,
    bar: null,
  });
}

/**
 * @param {Task[]} tasks
 * @param {ProgressScreen} progress
 * @returns {Task[]}
 */
function amendProgressBars(tasks, progress) {
  tasks.forEach((task) => {
    const { ipaType } = task;
    task.bar = progress.bar({ item: ipaType.id, logUpdates: true });
  });
  return tasks;
}

/**
 * @param {Task[]} tasks
 * @returns {Task[]}
 */
function amendIpaFilenames(tasks) {
  tasks.forEach((task) => {
    const { ipaFile, ipaBasePath } = task;

    if (ipaFile !== false) {
      const parsedIpaName = parse(ipaFile);
      parsedIpaName.dir = ipaBasePath;
      parsedIpaName.base = '';
      parsedIpaName.name += 'Signed';
      task.signedIpaFile = format(parsedIpaName);
    }
  });
  return tasks;
}

/**
 * @param {Task} task
 * @returns {Promise<boolean>}
 */
async function isTaskRequired(task) {
  const {
    ipaType,
    ipaBasePath,
    ipaFile,
    signedIpaFile,
    bar,
  } = task;

  if (ipaFile === false) {
    bar.error(`No unsigned ${ipaType.friendlyName} IPA found in ${ipaBasePath}`);
    return false;
  }

  // Check if signed IPA already exists
  const signedIpaExists = !!await stat(signedIpaFile).catch(() => false);
  if (signedIpaExists) {
    bar.done(`Found already signed IPA ${signedIpaFile}`);
    return false;
  }

  bar.log(`Unsigned IPA for ${ipaType.friendlyName} found, proceed signing`);
  return true;
}

/**
 * @param {Task} task
 * @returns {Promise<boolean>}
 */
async function resignIpa(task) {
  const {
    ipaFile,
    signedIpaFile,
    ipaType,
    provisioningProfile,
    developerCertificate,
    version,
    bar,
  } = task;

  const meter = new ProgressMeter({ progressBar: bar });

  const ipaSigner = new IpaSigner(provisioningProfile, developerCertificate, { meter });

  // Create IpaConfigurator with the list of IpaConfiguratorAction.
  // Create an IpaConfiguratorAction to update version if needed
  const configBaseDir = join(CONFIG_BASE_DIR, 'ipa', ipaType.id);
  const actions = [...ipaType.config];
  if (version) {
    actions.push(new VersionAction({ version }));
  }
  const ipaConfigurator = new IpaConfigurator(configBaseDir, actions);

  try {
    await ipaSigner.resignPackedIpa(ipaFile, signedIpaFile, ipaConfigurator);
    return true;
  } catch (/** @type {any} */ e) {
    bar.fail(e);
    return false;
  }
}

/**
 * Resigns one or more IPA according to the parameters
 * @param {Object} params
 * @param {import('./ReduxConfig').default} params.reduxConfig
 * @param {?string} params.ipa If not null, sign this specific IPA file
 * @param {?string} params.type Specify IPA type if `ipa` parameter is specified
 * @param {string[]} params.deviceTypes Sign the most recent IPAs of these types if `ipa`
 *                                      parameter is not specified
 * @param {?string} params.version If give, override the IPA version
 */
export default async function resign({
  reduxConfig,
  ipa = null,
  type = null,
  deviceTypes = [],
  version = null,
}) {
  const { provisioningProfile, developerCertificate, ipaTypeList } = reduxConfig;

  const provisioningProfileExists = !!await stat(provisioningProfile).catch(() => false);
  if (!provisioningProfileExists) throw new Error(`Missing mobile provisioning profile: ${provisioningProfile}`);

  const keychain = new Keychain();
  await keychain.unlockKeychain(reduxConfig.keychainPassword);

  const progress = new ProgressScreen(type !== null ? [type] : deviceTypes, { title: 'Re-sign IPAs' });

  // Build a task list, each item containing IPA type, path and progress bar
  /** @type {Task[]} */
  let tasks = [];
  if (ipa !== null && type !== null) {
    const ipaType = ipaTypeList.get(type);
    tasks.push({
      ipaType,
      type,
      ipaFile: ipa,
      bar: null,
    });
  } else {
    tasks = await Promise.all(
      deviceTypes.map((ipaType) => createTask(ipaTypeList.get(ipaType))),
    );
  }

  // Add ProgressBar, target IPA filenames (signed) and some more infos to each task
  tasks = amendProgressBars(tasks, progress);
  tasks = amendIpaFilenames(tasks);
  version = version || reduxConfig.versionOverride;
  tasks = tasks.map(
    (task) => ({
      ...task,
      provisioningProfile,
      developerCertificate,
      version,
    }),
  );

  // Keep only tasks for existing IPA files
  tasks = await pFilter(tasks, isTaskRequired);

  // Re-sign them
  await Promise.allSettled(tasks.map(resignIpa));

  progress.close();
}
