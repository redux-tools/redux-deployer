import { createWriteStream } from 'fs';
import { pipeline } from 'stream';
import { promisify } from 'util';
import fetch from 'node-fetch';

const streamPipeline = promisify(pipeline);

export default class HttpDownloader {

  /**
   * @param {string} url
   * @param {string} localPath
   * @param {function} progressCb
   * @returns {Promise<void>} Fulfills on successful download
   */
  // eslint-disable-next-line class-methods-use-this
  async download(url, localPath, progressCb = () => {}) {
    // Setup target first
    const outstream = createWriteStream(localPath);

    const response = await fetch(url);
    if (!response.ok) throw new Error(`Failed to download: ${response.statusText}`);

    // Tap in progress callback
    let bytesLoaded = 0;
    const bytesTotal = Number(response.headers.get('content-length'));
    response.body.on('data', (chunk) => {
      bytesLoaded += chunk.length;
      progressCb({ percentage: (bytesLoaded / bytesTotal) });
    });
    response.body.on('end', () => {
      progressCb({ percentage: 1 });
    });

    return streamPipeline(response.body, outstream);
  }

}
