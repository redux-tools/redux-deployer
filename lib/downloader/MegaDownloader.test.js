import { tmpdir } from 'os';
import { join } from 'path';
import { mkdtemp, rm, stat } from 'fs/promises';
import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import { step } from 'mocha-steps';
import MegaDownloader from './MegaDownloader.js';

use(chaiAsPromised);
use(sinonChai);

const TEST_FILE_URL = 'https://mega.nz/file/XRICCZzA#-qU7sPYlYywWIaH0MN035okyegUKwDMescMHufQ7IpU';
const INVALID_URL = 'https://mega.nz/file/XRICCZzA#-s9gS4t40GSgsdrgdhSr';

describe('MegaDownloader', () => {
  // eslint-disable-next-line func-names
  describe('#download()', function () { // no fat arrow here to preserve "this"
    this.timeout(6000);

    /** @type {string} */
    let tmpDir;
    /** @type {string} */
    let targetFile;
    /** @type {string} */
    let unwritableTargetFile;

    before(async () => {
      tmpDir = await mkdtemp(join(tmpdir(), 'megadltest-'));
      targetFile = join(tmpDir, 'download.txt');
      unwritableTargetFile = join(tmpDir, 'non_existing_subdir', 'unwritable.txt');
    });

    after(async () => {
      await rm(tmpDir, { recursive: true });
    });

    describe('should work with valid mega.nz address', () => {
      const downloader = new MegaDownloader();
      const progressCb = sinon.spy();

      step(
        'should resolve with success',
        () => expect(downloader.download(TEST_FILE_URL, targetFile, progressCb)).to.be.fulfilled,
      );

      step(
        'should have called the progress callback at least once',
        () => expect(progressCb.callCount).to.be.gte(1),
      );

      step(
        'should have a percentage of 1.0 in the last progress callback',
        () => expect(progressCb.lastCall.args[0]).to.be.an('object').with.property('percentage').which.is.equal(1.0),
      );

      step('should have created the local file', async () => {
        const fst = await stat(targetFile);
        return expect(fst.size).to.be.equal(15);
      });
    });

    it('should fail with valid mega.nz address but unwritable target', async () => {
      const downloader = new MegaDownloader();
      return expect(downloader.download(TEST_FILE_URL, unwritableTargetFile)).to.be.rejected;
    });

    it('should fail with invalid mega.nz addreess', () => {
      const downloader = new MegaDownloader();
      return expect(downloader.download(INVALID_URL, targetFile)).to.be.rejected;
    });

  });
});
