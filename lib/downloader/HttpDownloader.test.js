import { tmpdir } from 'os';
import { join } from 'path';
import { mkdtemp, rm, stat } from 'fs/promises';
import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import { step } from 'mocha-steps';
import nock from 'nock';
import HttpDownloader from './HttpDownloader.js';

use(chaiAsPromised);
use(sinonChai);

describe('HttpDownloader', () => {
  // eslint-disable-next-line func-names
  describe('#download()', function () { // no fat arrow here to preserve "this"
    this.timeout(6000);

    /** @type {string} */
    let tmpDir;
    /** @type {string} */
    let targetFile;

    before(async () => {
      tmpDir = await mkdtemp(join(tmpdir(), 'dltest-'));
      targetFile = join(tmpDir, 'download.txt');
    });

    after(async () => {
      await rm(tmpDir, { recursive: true });
    });

    describe('should work with valid url', () => {
      const downloader = new HttpDownloader();
      const progressCb = sinon.spy();

      nock('http://localhost').get('/test.ipa').reply(200, '1234567890');

      step(
        'should resolve with success',
        () => expect(downloader.download('http://localhost/test.ipa', targetFile, progressCb)).to.be.fulfilled,
      );

      step(
        'should have called the progress callback at least once',
        () => expect(progressCb.callCount).to.be.gte(1),
      );

      step(
        'should have a percentage of 1.0 in the last progress callback',
        () => expect(progressCb.lastCall.args[0]).to.be.an('object').with.property('percentage').which.is.equal(1.0),
      );

      step('should have created the local file', async () => {
        const fst = await stat(targetFile);
        return expect(fst.size).to.be.equal(10);
      });
    });

    it('should fail when ', () => {
      const downloader = new HttpDownloader();

      nock('http://localhost').get('/invalid.ipa').reply(404);

      return expect(downloader.download('http://localhost/invalid.ipa', targetFile)).to.be.rejected;
    });
  });
});
