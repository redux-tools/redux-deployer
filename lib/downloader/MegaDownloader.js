import { createWriteStream } from 'fs';
import mega from 'megajs';

const { File } = mega;

export default class MegaDownloader {

  /**
   * @param {string} megaUrl
   * @param {string} localPath
   * @param {function} progressCb
   * @returns {Promise<undefined>} Fulfills on successful download
   */
  // eslint-disable-next-line class-methods-use-this
  async download(megaUrl, localPath, progressCb = () => {}) {
    // Setup target first
    const outstream = createWriteStream(localPath);
    const remoteFile = File.fromURL(megaUrl);

    // Patch me if you can:
    // td;dr monkeypatching is actually a no-go, but here's no other way.
    // `megajs.File.download` sends an initial request to the Mega API to pull
    // some metadata. Unfortunately, it will start downloading unconditionally
    // when this request's callback is triggered, even if the `ReadableStream`
    // returned by `download` is already closed or destroyed.
    // Therefore, we will intercept all API calls, check whether it is one of these
    // initial metadata requests, and if so, we store the request objects to call
    // their `abort()` method if needed.
    /** @type {import('request').default[]} */
    const megaApiRequests = [];
    const origRequestModule = remoteFile.api.requestModule; // Store original module
    remoteFile.api.requestModule = (...params) => {
      const request = origRequestModule(...params); // Call original module first
      const options = params[0];
      // The initial API request object has an array named `json`, containing one
      // object that has either a key `p` or `n`, corresponding to the mega file's
      // `downloadId` or `nodeId`, respectively.
      if (typeof options === 'object'
          && Array.isArray(options.json)
          && typeof options.json[0] === 'object'
          && (
            (remoteFile.downloadId && options.json[0].p === remoteFile.downloadId)
            || (remoteFile.nodeId && options.json[0].n === remoteFile.nodeId)
          )
      ) {
        // Store me, I'm an initial API request
        megaApiRequests.push(request);
      }
      return request;
    };

    // Start streaming
    const instream = remoteFile.download({ maxConnections: 4 });
    instream.on('progress', ({ bytesLoaded, bytesTotal }) => {
      progressCb({ percentage: (bytesLoaded / bytesTotal) });
    });
    instream.pipe(outstream);

    return new Promise((res, rej) => {
      outstream.on('close', res);
      instream.on('error', rej);
      outstream.on('error', rej);
    }).catch((e) => {
      // Make sure any lingering `megajs` API request is aborted.
      megaApiRequests.forEach((req) => req.abort());
      // Sashay away, streams!
      instream.destroy();
      outstream.destroy();
      throw e;
    });
  }

}
