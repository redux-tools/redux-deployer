import { promisify } from 'util';
import { createWriteStream } from 'fs';
import { execFile as execFileCb } from 'child_process';
import { join, basename } from 'path';
import archiver from 'archiver';
import fastglob from 'fast-glob';
import ProgressMeter from '../util/ProgressMeter.js';

const execFile = promisify(execFileCb);

const UNZIP_BINARY = join('/usr', 'bin', 'unzip');

export default class IpaPacker {

  /** @type {string} */
  #unzipBinary;

  /**
   * @param {Object} options
   * @param {string} options.unzipBinary
   */
  constructor({ unzipBinary = UNZIP_BINARY } = {}) {
    this.#unzipBinary = unzipBinary;
  }

  /**
   * Unpacks ipaFile to a new temporary directory
   * @param {string} source Source archive
   * @param {string} target Target directory to unpack into
   * @returns {Promise<string>} Target directory to unpack into
   */
  async unpack(source, target) {
    await execFile(this.#unzipBinary, ['-q', '-o', source, '-d', target]);
    return target;
  }

  /**
   * @param {string} source Directory to pack
   * @param {string} target Target archive
   * @param {ProgressMeter} meter
   * @returns {Promise<string>} Target archive
   */
  // eslint-disable-next-line class-methods-use-this
  async packRecursive(source, target, meter = new ProgressMeter()) {
    const archive = archiver('zip');
    const output = createWriteStream(target);

    // Get a count on all files to convey progress
    const files = await fastglob('**', { cwd: source, onlyFiles: false });
    archive.on('progress', ({ entries }) => {
      const percentage = entries.processed / files.length;
      const file = files[entries.processed - 1] || '';
      meter.set({ percentage, message: `Pack ${basename(file)}`, file, log: false });
    });

    return new Promise((resolve, reject) => {
      archive.on('error', (err) => reject(err));
      output.on('close', () => resolve(target));

      archive.pipe(output);
      archive.directory(source, false);
      archive.finalize();
    });
  }

}
