import Toggle from 'enquirer/lib/prompts/toggle.js';

export default class YesNoToggle extends Toggle {

  /**
   * @param {Object} options
   */
  constructor({ ...options } = {}) {
    super({
      ...options,
      enabled: 'Yes',
      disabled: 'No',
    });
  }

}
