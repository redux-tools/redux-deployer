import YesNoToggle from './YesNoToggle.js';
import Menu from './Menu.js';
import DeviceSelect from './DeviceSelect.js';
import IpaTypeSelect from './IpaTypeSelect.js';
import InteractionCancelError from './InteractionCancelError.js';
import InteractionExitError from './InteractionExitError.js';
import { deploy } from '../redux/ReduxDeployAction.js';
import download from '../redux/ReduxDownloadAction.js';
import { reboot } from '../redux/ReduxRebootAction.js';
import remove from '../redux/ReduxRemoveAction.js';
import resign from '../redux/ReduxResignAction.js';

export default class InteractionController {

  /** @type {?string} */
  #optionIpa;

  /** @type {?string} */
  #optionVersion;

  /** @type {import('../ReduxConfig').default} */
  #reduxConfig;

  /** @type {?import('enquirer').Prompt|DeviceSelect|IpaTypeSelect|Menu|YesNoToggle} */
  #currentPrompt;

  /**
   * @param {Object} options
   * @param {?string} options.optionIpa
   * @param {?string} options.optionVersion
   * @param {import('../ReduxConfig').default} options.reduxConfig
   */
  constructor({
    optionIpa = null,
    optionVersion = null,
    reduxConfig,
  } = {}) {
    this.#optionIpa = optionIpa;
    this.#optionVersion = optionVersion;
    this.#reduxConfig = reduxConfig;
    this.#currentPrompt = null;
  }

  /**
   * Starts the interactive interation controller.
   * Returns normally if the user wants to exit, throws in case of an error
   * @throws {Error}
   */
  async run() {
    // Loop main menu
    for (;;) {
      try {
        // eslint-disable-next-line no-await-in-loop
        await this.#mainInteraction();
      } catch (e) {
        if (e instanceof InteractionExitError) {
          return;
        }
        if (!(e instanceof InteractionCancelError)) {
          throw e;
        }
      }
    }
  }

  /**
   * @returns {import('../models/DeviceList').default}
   */
  get #deviceList() {
    return this.#reduxConfig.deviceList;
  }

  /**
   * @returns {import('../models/IpaTypeList').default}
   */
  get #ipaTypeList() {
    return this.#reduxConfig.ipaTypeList;
  }

  /**
   * Returns the current prompt (or null if none is shown).
   * Mainly useful for testing.
   * @returns {?import('enquirer').Prompt|DeviceSelect|IpaTypeSelect|Menu|YesNoToggle}
   */
  get currentPrompt() {
    return this.#currentPrompt;
  }

  /**
   * Executes `run` on the given enquirer `prompt`, but also registeres it in `#currentPrompt`.
   * @param {import('enquirer').Prompt|DeviceSelect|IpaTypeSelect|Menu|YesNoToggle} prompt
   * @returns {Promise<any>}
   * @throws {InteractionCancelError} to get back to main menu
   * @throws {InteractionExitError} to exit
   */
  async #runEnquirerPrompt(prompt) {
    try {
      this.#currentPrompt = prompt;
      return await prompt.run();
    } finally {
      this.#currentPrompt = null;
    }
  }

  /**
   * @throws {InteractionCancelError} to get back to main menu
   * @throws {InteractionExitError} to exit
   */
  async #mainInteraction() {
    const menu = new Menu({
      message: 'Deploy, Remove or Resign?',
      choices: [
        { name: 'resign', message: 'Resign IPA', handler: () => this.#resignInteraction() },
        { name: 'deploy', message: 'Deploy IPA to devices', handler: () => this.#deployInteraction() },
        { name: 'reboot', message: 'Reboot devices', handler: () => this.#rebootInteraction() },
        { name: 'remove', message: 'Remove app from devices', handler: () => this.#removeInteraction() },
        { name: 'exit', message: 'Exit', handler: () => { throw new InteractionExitError(); } },
      ],
    });

    /** @type {string} */
    const choice = await this.#runEnquirerPrompt(menu);
    /** @type {function} */
    const handler = menu.find(choice, 'handler');
    await handler();
  }

  /**
   * @throws {InteractionCancelError} to get back to main menu
   * @throws {InteractionExitError} to exit
   */
  async #resignInteraction() {
    if (this.#optionIpa) {
      console.log('Resign IPA specified via command line.');
      const select = new IpaTypeSelect({
        ipaTypeList: this.#ipaTypeList,
        message: 'Which type is this?',
        multiple: false,
        cancel: true,
      });
      /** @type {string} */
      const type = await this.#runEnquirerPrompt(select);
      await resign({
        reduxConfig: this.#reduxConfig,
        ipa: this.#optionIpa,
        type,
        version: this.#optionVersion,
      });
      return;
    }

    if (this.#deviceList.size === 0) {
      throw new Error('No devices configured. Setup your device file!');
    }

    // Get a list of IpaTypes based on the default IPAs in our device list,
    // but limit to those which have a download info (`latestIpa`) available.
    const deviceTypes = this.#deviceList.getIpaTypeIds();
    const ipaTypeList = this.#ipaTypeList
      .filter(deviceTypes)
      .filter(([_, ipaType]) => !!ipaType.latestIpa);

    // Throw an error if IPA type
    if (ipaTypeList.size === 0) {
      throw new Error('No devices with known IPA types configured.');
    }

    // Ask the user about downloading latest version(s) of said IpaTypes
    /** @type {string[]} */
    let downloadDeviceTypes = [];
    if (ipaTypeList.size === 1) {
      const ipaType = ipaTypeList.map()[0];
      const toggle = new YesNoToggle({ message: `Check for new ${ipaType.friendlyName} IPA?` });
      const choice = await this.#runEnquirerPrompt(toggle);
      if (choice) {
        downloadDeviceTypes.push(ipaType.id);
      }
    } else {
      const select = new IpaTypeSelect({
        ipaTypeList,
        message: 'Check for new IPAs?',
        multiple: true,
      });
      downloadDeviceTypes = await this.#runEnquirerPrompt(select);
    }

    // Perform download, if any
    if (downloadDeviceTypes.length > 0) {
      await download({
        reduxConfig: this.#reduxConfig,
        deviceTypes: downloadDeviceTypes,
      });
    }

    await resign({
      reduxConfig: this.#reduxConfig,
      deviceTypes,
      version: this.#optionVersion,
    });
  }

  /**
   * @throws {InteractionExitError} to exit
   */
  async #deployInteraction() {
    if (this.#deviceList.size === 0) {
      throw new Error('No devices configured. Setup your device file!');
    }

    if (this.#optionIpa) {
      console.log(`Deploy IPA specified via command line: ${this.#optionIpa}`);
    }

    const deviceSelect = new DeviceSelect({
      deviceList: this.#deviceList,
      hintDefaultIpaType: !this.#optionIpa,
    });
    /** @type {import('../models/DeviceList').default} */
    const deviceList = await this.#runEnquirerPrompt(deviceSelect);

    await deploy({
      reduxConfig: this.#reduxConfig,
      deviceList,
      ipa: this.#optionIpa,
    });
  }

  /**
   * @throws {InteractionExitError} to exit
   */
  async #rebootInteraction() {
    const deviceSelect = new DeviceSelect({ deviceList: this.#deviceList });
    /** @type {import('../models/DeviceList').default} */
    const deviceList = await this.#runEnquirerPrompt(deviceSelect);
    if (deviceList.size === 0) return;

    await reboot({ deviceList });
  }

  /**
   * @throws {InteractionExitError} to exit
   */
  async #removeInteraction() {
    const deviceSelect = new DeviceSelect({ deviceList: this.#deviceList });
    /** @type {import('../models/DeviceList').default} */
    const deviceList = await this.#runEnquirerPrompt(deviceSelect);
    if (deviceList.size === 0) return;

    const select = new IpaTypeSelect({
      ipaTypeList: this.#ipaTypeList,
      message: 'What do you want to remove?',
      multiple: false,
    });
    const key = await this.#runEnquirerPrompt(select);

    await remove({
      reduxConfig: this.#reduxConfig,
      deviceList,
      appId: key !== 'default' ? this.#ipaTypeList.get(key).appId : null,
    });
  }

}
