import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import Device from '../models/Device.js';
import DeviceList from '../models/DeviceList.js';
import InteractionController from './InteractionController.js';
import { disableEnquirerRender, enableEnquirerRender } from './TestHelper.js';

use(chaiAsPromised);

describe('interactive/InteractionController', () => {
  beforeEach(() => {
    disableEnquirerRender();
  });

  afterEach(() => {
    enableEnquirerRender();
  });

  describe('#run', () => {
    it('should exit without action on interaction sequence Main -> Exit', () => {
      const ic = new InteractionController();

      // Start the interaction (runs into a Promise)
      const runPromise = ic.run();

      // The first prompt is the main menu.
      // Execute interaction sequence once the `run` event is emitted
      const mainMenu = ic.currentPrompt;
      mainMenu.once('run', () => {
        mainMenu.end(); // Navigate to last item ("Exit")
        mainMenu.submit(); // Press Enter
      });

      return expect(runPromise).to.be.fulfilled;
    });

    it('fail on interaction sequence Main -> Deploy if no devices are configured', () => {
      const ic = new InteractionController({
        reduxConfig: {
          deviceList: new DeviceList(),
        },
      });

      // Start the interaction (runs into a Promise)
      const runPromise = ic.run();

      // The first prompt is the main menu.
      // Execute interaction sequence once the `run` event is emitted
      const mainMenu = ic.currentPrompt;
      mainMenu.once('run', async () => {
        mainMenu.submit(); // Press Enter on 1st item ("Resign")
      });

      return expect(runPromise).to.be.rejectedWith(/^No devices configured/);
    });

    it('fail on interaction sequence Main -> Deploy if no devices are configured', () => {
      const ic = new InteractionController({
        reduxConfig: {
          deviceList: new DeviceList(),
        },
      });

      // Start the interaction (runs into a Promise)
      const runPromise = ic.run();

      // The first prompt is the main menu.
      // Execute interaction sequence once the `run` event is emitted
      const mainMenu = ic.currentPrompt;
      mainMenu.once('run', async () => {
        mainMenu.down(); // Navigate to 2nd item ("Deploy")
        mainMenu.submit(); // Press Enter
      });

      return expect(runPromise).to.be.rejectedWith(/^No devices configured/);
    });

    it('should exit without action on interaction sequence Main -> Reboot -> Select none', () => {
      const ic = new InteractionController({
        reduxConfig: {
          deviceList: new DeviceList().set('a', new Device()),
        },
      });

      // Start the interaction (runs into a Promise)
      const runPromise = ic.run();

      // The first prompt is the main menu.
      // We execute test interactions once the `run` event is emitted.
      // Using `process.nextTick` to wait for the next interaction promise is unstable, but
      // we'll keep it like this until we have a better solution.
      const mainMenu = ic.currentPrompt;
      mainMenu.once('run', async () => {
        mainMenu.down();
        mainMenu.down(); // Navigate to 3rd item ("Reboot")
        await mainMenu.submit(); // Press Enter
        process.nextTick(async () => {
          const rebootMenu = ic.currentPrompt;
          await rebootMenu.submit(); // Press Enter without marking anything
          process.nextTick(async () => {
            const mainMenu2 = ic.currentPrompt;
            mainMenu2.end(); // Navigate to last item ("Exit")
            mainMenu2.submit(); // Press Enter
          });
        });
      });

      return expect(runPromise).to.be.fulfilled;
    });

    it('should exit without action on interaction sequence Main -> Remove -> Select none', () => {
      const ic = new InteractionController({
        reduxConfig: {
          deviceList: new DeviceList().set('a', new Device()),
        },
      });

      // Start the interaction (runs into a Promise)
      const runPromise = ic.run();

      // The first prompt is the main menu.
      // We execute test interactions once the `run` event is emitted.
      // Using `process.nextTick` to wait for the next interaction promise is unstable, but
      // we'll keep it like this until we have a better solution.
      const mainMenu = ic.currentPrompt;
      mainMenu.once('run', async () => {
        mainMenu.down();
        mainMenu.down();
        mainMenu.down(); // Navigate to 4th item ("Remove")
        await mainMenu.submit(); // Press Enter
        process.nextTick(async () => {
          const rebootMenu = ic.currentPrompt;
          await rebootMenu.submit(); // Press Enter without marking anything
          process.nextTick(async () => {
            const mainMenu2 = ic.currentPrompt;
            mainMenu2.end(); // Navigate to last item ("Exit")
            mainMenu2.submit(); // Press Enter
          });
        });
      });

      return expect(runPromise).to.be.fulfilled;
    });
  });
});
