import { MultiProgressBars } from 'multi-progress-bars';
import Device from '../models/Device.js';
import DeviceList from '../models/DeviceList.js';
import ProgressBar from './ProgressBar.js';

export default class ProgressScreen {

  /** @type {MultiProgressBars} */
  #bars;

  /** @type {string[]} */
  #items;

  /**
   * Creates a new ProgressScreen with the given items
   *
   * @param {string[]|DeviceList} items
   * @param {Object} options
   * @param {?string|boolean} options.title
   * @param {?boolean} options.preseed true if we should pre-create all progress bars
   */
  constructor(items, { title = false, preseed = false } = {}) {
    if (items instanceof DeviceList) {
      items = items.map((device) => device.name);
    }

    this.#bars = new MultiProgressBars({
      border: true,
      header: title,
      footer: true,
      persist: true,
    });
    this.#items = items;

    if (preseed) {
      this.#items.forEach((item) => this.#ensureProgressBarTask(item));
    }
  }

  /**
   * Finishes this `ProgressScreen`.
   */
  close() {
    this.#bars.close();
    console.log();
  }

  /**
   * @param {string} message
   */
  // eslint-disable-next-line class-methods-use-this
  log(message) {
    console.log(message);
  }

  /**
   * Returns a `ProgressBar` to display progress for the given item.
   * @param {Object} params
   * @param {string|Device} params.item
   * @param {number} params.attempts Number of retry attempts
   * @param {boolean} params.logUpdates Set to `true` if updates should be logged in the
   *                                    logging area
   * @returns {ProgressBar}
   */
  bar({ item, attempts = 1, logUpdates = false }) {
    if (item instanceof Device) {
      item = item.name;
    }
    this.#ensureProgressBarTask(item);
    return new ProgressBar({
      bars: this.#bars,
      name: item,
      logUpdates,
      attempts,
    });
  }

  /**
   * Removes the progress bar from the screen
   * @param {string|Device|ProgressBar} item
   */
  removeBar(item) {
    if (item instanceof Device || item instanceof ProgressBar) {
      item = item.name;
    }
    this.#bars.removeTask(item);
  }

  /**
   * Creates a "task" in the `MultiProgressBars` object if it doesn't exist so far
   * @param {string} task
   */
  #ensureProgressBarTask(task) {
    if (this.#bars.getIndex(task) === undefined) {
      this.#bars.addTask(task, { type: 'percentage' });
    }
  }

}
