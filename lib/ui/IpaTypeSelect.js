import Select from 'enquirer/lib/prompts/select.js';
import InteractionCancelError from './InteractionCancelError.js';

/**
 * @param {Object} options
 * @param {import('../models/IpaTypeList').default} options.ipaTypeList List of IPA types to display
 * @param {boolean} options.multiple true if multi selection enabled
 * @param {boolean} options.def true if an "Default" option should be prepended
 * @param {boolean} options.all true if an "All" option should be prepended
 * @param {boolean} options.cancel true if a "Cancel" option should be added
 * @returns {Object[]}
 */
function buildChoices({
  ipaTypeList, multiple, def, all, cancel,
}) {
  const result = ipaTypeList.map((ipaType, key) => ({
    name: key,
    message: ipaType.friendlyName,
  }));
  if (all && multiple && result.length) {
    result.unshift({ name: 'all', message: 'All' });
  }
  if (def && result.length) {
    result.unshift({ name: 'default', message: 'Default' });
  }
  if (cancel && result.length) {
    result.push({ name: 'cancel', message: 'Cancel' });
  }
  return result;
}

export default class IpaTypeSelect extends Select {

  /** @type {import('../models/IpaTypeList').default} */
  #ipaTypeList;

  /**
   * @param {Object} options
   * @param {import('../models/IpaTypeList').default} options.ipaTypeList List of IPA types to display
   * @param {boolean} options.multiple true if multi selection enabled
   * @param {boolean} options.def true if an "Default" option should be prepended
   * @param {boolean} options.all true if an "All" option should be prepended
   * @param {boolean} options.cancel true if a "Cancel" option should be added
   */
  constructor({
    ipaTypeList,
    multiple = true,
    def = false,
    all = false,
    cancel = false,
    ...options
  }) {
    super({
      ...options,
      pointer: '>>> ',
      multiple,
      choices: buildChoices({
        ipaTypeList, multiple, def, all, cancel,
      }),
    });

    this.#ipaTypeList = ipaTypeList;
  }

  /**
   * @returns {Promise<string|string[]>}
   * @throws {InteractionCancelError} if the user selected the 'Cancel' item
   */
  async run() {
    const { multiple } = this.options;

    const result = await super.run();

    // Look for special results
    const lastElement = multiple ? result[result.length - 1] : result;
    if (lastElement === 'cancel') {
      throw new InteractionCancelError();
    }
    const firstElement = multiple ? result[0] : result;
    if (firstElement === 'all') {
      return this.#ipaTypeList.map((_, key) => key);
    }

    return result;
  }

}
