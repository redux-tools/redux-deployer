/**
 * Thrown if the user wants to cancel operation
 */
export default class InteractionCancelError extends Error {
}
