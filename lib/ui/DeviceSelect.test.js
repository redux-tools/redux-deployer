import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import DeviceList from '../models/DeviceList.js';
import Device from '../models/Device.js';
import InteractionCancelError from './InteractionCancelError.js';
import DeviceSelect from './DeviceSelect.js';
import { disableEnquirerRender, enableEnquirerRender } from './TestHelper.js';

use(chaiAsPromised);

describe('interactive/DeviceSelect', () => {
  beforeEach(() => {
    disableEnquirerRender();
  });

  afterEach(() => {
    enableEnquirerRender();
  });

  describe('constructor', () => {
    const deviceList = new DeviceList();

    it('should work with default options', () => {
      const prompt = new DeviceSelect({ deviceList });
      expect(prompt.options.message).to.be.equal('Press spacebar to select devices.');
    });

    it('should work with a title (message) option', () => {
      const prompt = new DeviceSelect({ deviceList, message: 'title' });
      expect(prompt.options.message).to.be.equal('title');
    });
  });

  describe('#run', () => {
    /** @type {Device} */
    let deviceA;
    /** @type {Device} */
    let deviceB;
    /** @type {Device} */
    let deviceC;
    /** @type {DeviceList} */
    let deviceList;

    /**
     * @param {Object} options
     * @param {string[]} testInteractions Names of interaction methods which will be sequentially
     *                                    called on the prompt
     * @returns {DeviceSelect}
     */
    const setupTestDummy = (options, testInteractions) => {
      // Setup a few Devices
      deviceA = new Device('a');
      deviceB = new Device('b');
      deviceC = new Device('c');
      // Setup a DeviceList
      deviceList = new DeviceList().set('a', deviceA).set('b', deviceB).set('c', deviceC);
      // Setup actual DeviceSelect
      const prompt = new DeviceSelect({ deviceList, ...options });
      // Perform testInteractions once `run` event fires
      prompt.once('run', () => {
        testInteractions.forEach((el) => prompt[el]());
      });
      return prompt;
    };

    describe('multiple = false', () => {
      it('should return first menu item if submitted', () => {
        const prompt = setupTestDummy({ multiple: false }, ['submit']);
        return expect(prompt.run()).to.eventually.be.instanceOf(DeviceList).that.has.all.keys(['a']);
      });

      it('should return third menu item if submitted after 2 down presses', () => {
        const prompt = setupTestDummy({ multiple: false }, ['down', 'down', 'submit']);
        return expect(prompt.run()).to.eventually.be.instanceOf(DeviceList).that.has.all.keys(['c']);
      });

      it('should throw InteractionCancelError if "Cancel" is selected', () => {
        const prompt = setupTestDummy({ multiple: false, cancel: true }, ['end', 'submit']);
        return expect(prompt.run()).to.eventually.be.rejectedWith(InteractionCancelError);
      });
    });

    describe('multiple = true', () => {
      it('should return first and second menu item if submitted', () => {
        const prompt = setupTestDummy({ multiple: true, all: false }, ['space', 'down', 'space', 'submit']);
        return expect(prompt.run()).to.eventually.be.instanceOf(DeviceList).that.has.all.keys(['a', 'b']);
      });

      it('should return all items if "All" is selected', () => {
        const prompt = setupTestDummy({ multiple: true }, ['space', 'submit']);
        return expect(prompt.run()).to.eventually.be.instanceOf(DeviceList).that.has.all.keys(['a', 'b', 'c']);
      });

      it('should throw InteractionCancelError if "Cancel" is selected', () => {
        const prompt = setupTestDummy({ multiple: true, cancel: true }, ['end', 'space', 'submit']);
        return expect(prompt.run()).to.eventually.be.rejectedWith(InteractionCancelError);
      });
    });

    it('should reject if interaction is canceled', () => {
      const prompt = setupTestDummy({}, ['cancel']);
      return expect(prompt.run()).to.eventually.be.rejected;
    });

    it('should continue without error if a Device is updated', async () => {
      const prompt = setupTestDummy({}, []); // empty testInteractions array on purpose
      // Setup custom testInteractions
      prompt.once('run', () => {
        deviceB.port = 'usb'; // This triggers an update event on deviceB and updates of the menu item's "hint".
        deviceC.emit('update', deviceC); // This emulates an (currently impossible) update on a device with port = null.
        prompt.submit(); // Submit prompt
      });
      const result = await prompt.run();
      expect(prompt.find('b').hint).to.match(/usb/);
      expect(result).to.be.instanceOf(DeviceList).with.length(0);
    });
  });
});
