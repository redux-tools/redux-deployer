import ansiColors from 'ansi-colors';
import Select from 'enquirer/lib/prompts/select.js';
import InteractionCancelError from './InteractionCancelError.js';

const { gray } = ansiColors;

/**
 * Renders the given `DeviceList` (plus optional "All" and "Cancel" options) and awaits
 * user interaction for selection. It uses the `Device`'s name as menu item and its `port`
 * field as menu hint.
 * If the underlying `Device`s are updated (ie. setting its port), the menu hint is updated
 * accordingly.
 */
export default class DeviceSelect extends Select {

  /** @type {import('../models/DeviceList').default} */
  #deviceList;

  /** @type {boolean} */
  #hintDefaultIpaType;

  /**
   * @param {Object} options
   * @param {import('../models/DeviceList').default} options.deviceList List of Devices to display
   * @param {boolean} options.multiple true if multi selection enabled
   * @param {boolean} options.all true if an "All" option should be prepended
   * @param {boolean} options.cancel true if a "Cancel" option should be added
   * @param {string} options.message Title
   * @param {boolean} options.hintDefaultIpaType Hint includes the device's default IPA type
   */
  constructor({
    deviceList,
    multiple = true,
    all = true,
    cancel = false,
    message = 'Press spacebar to select devices.',
    hintDefaultIpaType = false,
    ...options
  }) {
    super({
      ...options,
      choices: [],
      pointer: '>>> ',
      multiple,
      message,
    });

    this.#deviceList = deviceList;
    this.#hintDefaultIpaType = hintDefaultIpaType;

    this.options.choices = this.#buildChoices({ multiple, all, cancel });
  }

  /**
   * @returns {Promise<import('../models/DeviceList').default>}
   * @throws {InteractionCancelError} if the user selected the 'Cancel' item
   */
  async run() {
    const { multiple } = this.options;

    // Since the `Devices` in the `DeviceList` can be updated at any time, we setup a
    // listener which can update this `Select` prompt accordingly.
    /** @param {import('../models/Device').default} device */
    const updateHandler = (device) => {
      const key = device.name;
      const choice = this.find(key);
      choice.hint = this.#getDeviceHint(device);
      this.render();
    };
    this.#deviceList.forEach((device) => device.addListener('update', updateHandler));
    const cleanup = () => this.#deviceList.forEach((device) => device.removeListener('update', updateHandler));
    this.once('close', cleanup);

    // Now with everything setup, let's run the `Select` prompt.
    const result = await super.run();

    // Look for special results
    const lastElement = multiple ? result[result.length - 1] : result;
    if (lastElement === 'cancel') {
      throw new InteractionCancelError();
    }
    const firstElement = multiple ? result[0] : result;
    if (firstElement === 'all') {
      return this.#deviceList;
    }

    return this.#deviceList.filter(multiple ? result : [result]);
  }

  /**
   * @param {import('../models/Device').default} device
   * @returns {string}
   */
  #getDeviceHint(device) {
    const hints = [];
    if (this.#hintDefaultIpaType) {
      hints.push(device.type);
    }
    hints.push(device.port || 'undetected');

    return gray(hints.join(', '));
  }

  /**
   * @param {Object} options
   * @param {boolean} options.multiple true if multi selection enabled
   * @param {boolean} options.all true if an "All" option should be prepended
   * @param {boolean} options.cancel true if a "Cancel" option should be added
   * @returns {Object[]}
   */
  #buildChoices({ multiple, all, cancel }) {
    const result = this.#deviceList.map((device, key) => ({
      name: key,
      message: device.name,
      hint: this.#getDeviceHint(device),
    }));
    if (all && multiple && result.length) {
      result.unshift({ name: 'all', message: 'All' });
    }
    if (cancel && result.length) {
      result.push({ name: 'cancel', message: 'Cancel' });
    }
    return result;
  }

}
