import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import InteractionCancelError from './InteractionCancelError.js';
import InteractionExitError from './InteractionExitError.js';
import Menu from './Menu.js';
import { disableEnquirerRender, enableEnquirerRender } from './TestHelper.js';

use(chaiAsPromised);

describe('interactive/Menu', () => {
  beforeEach(() => {
    disableEnquirerRender();
  });

  afterEach(() => {
    enableEnquirerRender();
  });

  const setupTestDummy = (options, testInteractions) => {
    const choices = ['a', 'b', 'c', 'd'];
    const prompt = new Menu({ choices, ...options });
    // Perform testInteractions once `run` event fires
    prompt.once('run', () => {
      testInteractions.forEach((el) => prompt[el]());
    });
    return prompt;
  };

  describe('#run', () => {
    it('should return first menu item if submitted', () => {
      const prompt = setupTestDummy({}, ['submit']);
      return expect(prompt.run()).to.eventually.be.equal('a');
    });

    it('should return third menu item if submitted after 2 down presses', () => {
      const prompt = setupTestDummy({}, ['down', 'down', 'submit']);
      return expect(prompt.run()).to.eventually.be.equal('c');
    });

    it('should throw InteractionCancelError if "Cancel" is selected', () => {
      const prompt = setupTestDummy({ cancel: true }, ['end', 'submit']);
      return expect(prompt.run()).to.eventually.be.rejectedWith(InteractionCancelError);
    });

    it('should throw InteractionExitError if "Exit" is selected', () => {
      const prompt = setupTestDummy({ exit: true }, ['down', 'down', 'down', 'down', 'submit']);
      return expect(prompt.run()).to.eventually.be.rejectedWith(InteractionExitError);
    });

    it('should reject if interaction is canceled', () => {
      const prompt = setupTestDummy(undefined, ['cancel']);
      return expect(prompt.run()).to.eventually.be.rejected;
    });
  });
});
