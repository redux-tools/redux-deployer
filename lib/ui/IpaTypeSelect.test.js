import { expect, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import InteractionCancelError from './InteractionCancelError.js';
import IpaTypeSelect from './IpaTypeSelect.js';
import { disableEnquirerRender, enableEnquirerRender } from './TestHelper.js';

use(chaiAsPromised);

describe('interactive/IpaTypeSelect', () => {
  beforeEach(() => {
    disableEnquirerRender();
  });

  afterEach(() => {
    enableEnquirerRender();
  });

  const setupTestDummy = (options, testInteractions) => {
    // Mock an IpaTypeList
    const ipaTypeList = new Map()
      .set('a', { friendlyName: 'A' })
      .set('b', { friendlyName: 'B' })
      .set('c', { friendlyName: 'C' });
    function map(fn) {
      return [...this].map(([key, ipaType]) => fn(ipaType, key));
    }
    ipaTypeList.map = map;
    // Setup IpaTypeSelect
    const prompt = new IpaTypeSelect({ ipaTypeList, ...options });
    // Perform testInteractions once `run` event fires
    prompt.once('run', () => {
      testInteractions.forEach((el) => prompt[el]());
    });
    return prompt;
  };

  describe('#run', () => {
    describe('multiple = false', () => {
      it('should return first menu item if submitted', () => {
        const prompt = setupTestDummy({ multiple: false }, ['submit']);
        return expect(prompt.run()).to.eventually.be.equal('a');
      });

      it('should return third menu item if submitted after 2 down presses', () => {
        const prompt = setupTestDummy({ multiple: false }, ['down', 'down', 'submit']);
        return expect(prompt.run()).to.eventually.be.equal('c');
      });

      it('should return "default" if "Default" is selected', () => {
        const prompt = setupTestDummy({ multiple: false, def: true }, ['submit']);
        return expect(prompt.run()).to.eventually.be.equal('default');
      });

      it('should throw InteractionCancelError if "Cancel" is selected', () => {
        const prompt = setupTestDummy({ multiple: false, cancel: true }, ['end', 'submit']);
        return expect(prompt.run()).to.eventually.be.rejectedWith(InteractionCancelError);
      });
    });

    describe('multiple = true', () => {
      it('should return first and second menu item if submitted', () => {
        const prompt = setupTestDummy({ multiple: true }, ['space', 'down', 'space', 'submit']);
        return expect(prompt.run()).to.eventually.be.deep.equal(['a', 'b']);
      });

      it('should return all items if "All" is selected', () => {
        const prompt = setupTestDummy({ multiple: true, all: true }, ['space', 'submit']);
        return expect(prompt.run()).to.eventually.be.deep.equal(['a', 'b', 'c']);
      });

      it('should throw InteractionCancelError if "Cancel" is selected', () => {
        const prompt = setupTestDummy({ multiple: true, cancel: true }, ['end', 'space', 'submit']);
        return expect(prompt.run()).to.eventually.be.rejectedWith(InteractionCancelError);
      });
    });

    it('should reject if interaction is canceled', () => {
      const prompt = setupTestDummy(undefined, ['cancel']);
      return expect(prompt.run()).to.eventually.be.rejected;
    });
  });
});
