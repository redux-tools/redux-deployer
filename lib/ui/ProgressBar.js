export default class ProgressBar {

  /** @type {import('multi-progress-bars').MultiProgressBars} */
  #bars;

  /** @type {string} */
  #name;

  /** @type {boolean} */
  #logUpdates;

  /** @type {number} */
  #attempts;

  /** @type {?number} */
  #currentAttempt;

  /**
   * Creates a ProgressBar instance, to be used from ProgressScreen only
   * @param {Object} params
   * @param {import('multi-progress-bars').MultiProgressBars} params.bars
   * @param {string} params.name
   * @param {number} params.attempts
   * @param {boolean} params.logUpdates Set to `true` if updates should be logged in the
   *                                    logging area
   */
  constructor({
    bars,
    name,
    logUpdates = false,
    attempts = 1,
  }) {
    this.#bars = bars;
    this.#name = name;
    this.#logUpdates = logUpdates;
    this.#attempts = attempts;
    this.#currentAttempt = 1;
  }

  /**
   * @returns {string}
   */
  get name() {
    return this.#name;
  }

  /**
   * Advances the progress bar with a message, optionally logs that message
   * @param {number|{percentage:number,message:string,log:boolean}} percentage Progress in
   *        percentage (0.0-1.0), or object with percentage and message
   * @param {string} message
   * @param {?boolean} log Additionally log the update to the logging area (`null` =
   *                       constructor's default)
   */
  update(percentage, message = '', log = null) {
    // Adjust parameter if an object is passed in
    if (typeof percentage === 'object') {
      log = percentage.log;
      message = percentage.message;
      percentage = percentage.percentage;
    }

    message = this.#attemptify(message);
    if (log || (log === null && this.#logUpdates)) {
      console.log(`${this.#name}: ${message}`);
    }
    this.#bars.updateTask(this.#name, { percentage, message });
  }

  /**
   * Logs a message without advancing in the progress bar
   * @param {string} message
   */
  log(message) {
    message = this.#attemptify(message);
    this.#bars.updateTask(this.#name, { message });
    console.log(`${this.#name}: ${message}`);
  }

  /**
   * Logs an error without advancing in the progress bar
   * @param {string|Error} message
   * @param {?Error} exception
   */
  error(message, exception = null) {
    message = this.#erronify(message, exception);
    message = this.#attemptify(message);
    this.#bars.updateTask(this.#name, { message });
    console.error(`${this.#name}: ${message}`, exception || '');
  }

  /**
   * Logs the final message for a successful progress
   * @param {string} message
   */
  done(message) {
    message = this.#attemptify(message);
    this.#bars.done(this.#name, { message });
    console.log(`${this.#name}: ${message}`);
  }

  /**
   * Logs the final message for a erroneous progress
   * @param {string|Error} message
   * @param {?Error} exception
   */
  fail(message, exception = null) {
    message = this.#erronify(message, exception);
    message = this.#attemptify(message);
    this.#bars.done(this.#name, { message });
    console.error(`${this.#name}: ${message}`, exception || '');
  }

  /**
   * In a multi-attempt bar, proceed to the next attempt
   */
  retryNext() {
    if (this.#currentAttempt !== null) {
      this.#currentAttempt++;
      if (this.#currentAttempt > this.#attempts) {
        this.#currentAttempt = null;
      }
    }
  }

  /**
   * In a multi-attempt bar, mark that we no longer retry
   */
  retryDone() {
    this.#currentAttempt = null;
  }

  /**
   * Prepends the message with the current attempt
   * @param {string} message
   * @returns {string}
   */
  #attemptify(message) {
    return (this.#attempts > 1 && this.#currentAttempt !== null)
      ? `(#${this.#currentAttempt}/${this.#attempts}) ${message}`
      : message;
  }

  /**
   * Tries to make the best formatting if the message is an `Error` or an additional `Error`
   * has been passed in.
   * @param {string|Error} message
   * @param {?Error} exception
   * @returns {string}
   */
  // eslint-disable-next-line class-methods-use-this
  #erronify(message, exception) {
    if (message instanceof Error) {
      message = message.message;
    }
    if (!message && exception) {
      message = exception.message;
    }
    return message;
  }

}
